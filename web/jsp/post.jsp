<%@ page import="com.example.view.CategoryView" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Post</title>
      <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
      <script>tinymce.init({ selector:'textarea' });</script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  </head>
  <body>
  <%List<CategoryView> categoryList =(List<CategoryView>)request.getAttribute("categoryList");%>

    <div class="jumbotron">
        <form role="form" action="/Blog/PostServlet" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <textarea cols="100" rows="50" name="text" id="text">
                    </textarea>
                </div>
                <div class="form-group">
                    <label for="category">Category</label>
                    <select id="category" name="category">
                        <%for(CategoryView category:categoryList){%>
                            <option value="<%=category.getName()%>"><%=category.getName()%></option>
                        <%}%>
                    </select>
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" name="username" id="username" />
                </div>
                <div class="form-group">
                    <label for="document" >Document</label>
                    <input type="file" name="document" id="document" />
                </div>
                <div class="form-group">
                    <label for="documentType">DocumentType</label>
                    <input type="text" name="documentType" id="documentType" />
                </div>
            <button type="submit" class="btn btn-success">Add Post</button>
        </form>
  </div>
  </body>
</html>
