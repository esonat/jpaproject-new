<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="user.css"/>
</head>
<body>
<% String errorMessage=(String)request.getAttribute("errorMessage");%>

<h1><% if(errorMessage!=null){%><%=errorMessage%><%}%></h1>
<div style="background-color:black;color:white;">
<form role="form" action="/Blog/UserServlet" method="POST">
   <table>
        <tr>
            <td>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" value="name" name="name" id="name" class="form-control" placeholder="Name" aria-describedby="basic-addon1">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label for="city">City</label>
                    <input type="text" value="Istanbul" name="city" id="city" class="form-control" placeholder="City" aria-describedby="basic-addon1">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label for="country">Country</label>
                    <input type="text" value="Turkey" name="country" id="country" class="form-control" placeholder="Country" aria-describedby="basic-addon1">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label for="street">Street</label>
                    <input type="text" value="street" name="street" id="street" class="form-control" placeholder="Street" aria-describedby="basic-addon1">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label for="state">State</label>
                    <input type="text" value="acibadem" name="state" id="state" class="form-control" placeholder="State" aria-describedby="basic-addon1">
                </div>
        <tr>
       <td>
           <div class="form-group">
               <label for="zip">Zip</label>
               <input type="text" value="zip" name="zip" id="zip" class="form-control" placeholder="Zip" aria-describedby="basic-addon1">
           </div>
           </td>
       </tr>
       <tr>
           <td>
               <div class="form-group">
                   <label for="username">Username</label>
                   <input type="text" value="user" name="username" id="username" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
               </div>
           </td>
       </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="text" value="user" name="password" id="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="text" value="12345" name="phone" id="phone" class="form-control" placeholder="Phone" aria-describedby="basic-addon1">
                </div>
            </td>
       </tr>
        <tr>
            <td>
                <button type="submit" class="btn btn-success">Add User</button>
            </td>
        </tr>
   </table>
</form>
</div>
</body>
</html>
