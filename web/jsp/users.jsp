<%@ page import="com.example.entities.User" %>
<%@ page import="java.util.List" %>
<%@ page import="com.example.entities.UserInfo" %>
<%@ page import="com.example.entities.UserLoginInfo" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="true" %>
<html>
<head>
    <title>Users</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<%
  List<User> users=(List<User>)request.getAttribute("users");
%>
<div class="jumbotron">
    <table class="table table-inverse">
        <thead>
        <tr>
            <th>Name</th>
            <th>Username</th>
            <th>Password</th>
        </tr>
        </thead>
        <tbody>
    <%    for(User user:users){%>
            <tr>
                <td>
                <%
                    UserInfo userInfo=(UserInfo)user.getUserinfo();
                    String name=(String)userInfo.getName();
                %><%=name%>
            </td>
            <td>
                <%

                   String username= (String)((UserLoginInfo)user.getUserLoginInfo()).getUsername();
                %><%=username%>
            </td>
            <td>
                <%
                    String password=(String)((UserLoginInfo)user.getUserLoginInfo()).getPassword();
                %>
                <%=password%>
            </td>
    </tr>
        <% } %>
        </tbody>
</table>
    </div>
</body>
</html>
