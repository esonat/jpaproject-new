package servlet;

import com.example.entities.*;
import com.example.service.BlogServiceRemote;
import com.example.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonat on 10.08.2016.
 */
//@WebServlet(name="UserServlet",urlPatterns = "/UserServlet")
public class UserServlet extends HttpServlet {
    private static final Logger logger= LoggerFactory.getLogger(UserServlet.class);
    private static String LNAME="USERSERVLET";

    @Inject
    BlogServiceRemote blogService;
    @Inject
    TestService testService;

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            String name = request.getParameter("name");
            String city = request.getParameter("city");
            String country = request.getParameter("country");
            String street = request.getParameter("street");
            String state = request.getParameter("state");
            String zip = request.getParameter("zip");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String phone = request.getParameter("phone");

            List<Phone> phones = new ArrayList<Phone>();
            Phone phone1 = new Phone();
            phone1.setNum(phone);


            City cityItem = blogService.getCityByName(city);

            Address address = new Address();
            address.setCity(cityItem);
            address.setZip(zip);
            address.setStreet(street);
            address.setState(state);

            UserInfo userInfo = new UserInfo();
            userInfo.setName(name);
            userInfo.setAddress(address);
            userInfo.setPhones(phones);

            UserLoginInfo userLoginInfo = new UserLoginInfo();
            userLoginInfo.setUsername(username);
            userLoginInfo.setPassword(password);

            User user = new User();
            user.setUserinfo(userInfo);
            user.setUserLoginInfo(userLoginInfo);

            List<String> roles = new ArrayList<String>();

            roles.add("Admin");
            roles.add("PostUser");

            blogService.addUser(user, roles, phones);
        }catch (EJBException e){
            logger.error(LNAME+"doPost"+e.getCause()+e.getMessage());
        }catch (ConstraintViolationException e){
            logger.error(LNAME+"doPost"+e.getCause()+e.getMessage());
        }catch (Exception e){
            logger.error(LNAME+"doPost"+e.getCause()+e.getMessage());
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException,IOException{
    try {
        String action = request.getParameter("action");

        if (request.getSession().getAttribute("errorMessage") != null) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/jsp/user.jsp");
            rd.forward(request, response);
        }

        if (action.equals("list")) {
            List<User> users = blogService.getAllUsers();
            request.setAttribute("users", users);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/jsp/users.jsp");
            rd.forward(request, response);
        } else if (action.equals("add")) {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/jsp/user.jsp");
            rd.forward(request, response);
        } else if (action.equals("role")) {
            String role = "notexists";
            if (blogService.findRoleByName("Admin") != null) {
                role = "exists";
            }
            request.setAttribute("role", role);
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/jsp/role.jsp");
            rd.forward(request, response);
        } else if (action.equals("test")) {
            testService.test();
        }

        }catch (Exception e){
            logger.error(LNAME+"doGet"+e.getCause()+e.getMessage());
        }
    }
}
