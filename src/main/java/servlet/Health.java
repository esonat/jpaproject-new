package servlet;

import com.example.connection.ClientManagerRemote;
import com.example.entities.*;
import com.example.exception.CategoryNotFoundException;
import com.example.exception.RepositoryException;
import com.example.exception.UserNotFoundException;
import com.example.service.BlogServiceRemote;
import com.example.task.TaskManagerRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class Health extends HttpServlet{

    @EJB
    ClientManagerRemote clientManager;
    @EJB
    TaskManagerRemote taskManager;
    @EJB
    BlogServiceRemote blogService;

    private static final String LNAME="HEALTH";
    Logger logger= LoggerFactory.getLogger(Health.class);

    public void HeartBeatRoutine(HttpServletRequest request,HttpServletResponse response)
    throws IOException {

        String clientId = request.getParameter("id");
        String port_str = request.getParameter("port");
        String username = request.getParameter("username");
        String role = request.getParameter("role");

        if (clientId == null) {
            logger.warn("CLIENTID CANNOT BE NULL");
        }
        if (port_str == null) {
            logger.warn("PORT CANNOT BE NULL");
        }
        if (username == null) {
            logger.warn("USERNAME CANNOT BE NULL");
        }
        if (role == null) {
            logger.warn("ROLE CANNOT BE NULL");
        }


//        for(RestClient item:clientManager.getConnectedClients()){
//            //logger.info("CLIENTLIST:"+item.toString());
//        }

        //  try {
//        Client client=null;
//
//        try{
//            client = clientManager.getClientById(clientId);
//        }catch (Exception e){
//            logger.error(e.getCause()+e.getMessage());
//            //logger.error(LNAME+" - HeartBeatRoutine - Error in getting client by id.ClientId:"+clientId);
//            return;
//        }

        //if (client == null) {


        try {
            clientManager.addClient(clientId, port_str, username, role);

        }catch (UserNotFoundException e){
            logger.error(e.getMessage());
            return;
        }
//        }else {
//
//            client.setPort(Integer.parseInt(port));
//            User user=null;
//
//            try {
//                user = blogService.getUserByUsername(username);
//
//            }catch (Exception e){
//                logger.error(e.getCause()+e.getMessage());
//                //logger.error(LNAME+" - HeartBeatRoutine - Error in getting user by username.ClientId:"+clientId+" username:"+username);
//                return;
//            }
//
//            if (user == null) {
//                logger.warn(LNAME + " - HeartBeatRoutine - User with username:" + username + "not found");
//                return;
//            }
//
//            client.setUser(user);
//            client.setClientRole(role);
//
//            try{
//                clientManager.updateClient(client,true);
//                //logger.info(LNAME+MNAME+" CLIENT UPDATED");
//
//            }catch (ClientNotFoundException e){
//                logger.error(e.getCause()+e.getMessage());
//                //logger.error(LNAME+MNAME+e.getCause()+e.getMessage());
//                return;
//
//            }catch(Exception e){
//                logger.error(e.getCause()+e.getMessage());
//                //logger.error(LNAME+MNAME+e.getCause()+e.getMessage());
//                return;
//            }
    // }
    }

    public void PostRoutine(HttpServletRequest request,HttpServletResponse response)
        throws IOException{

            String clientId = request.getParameter("id");
            String text     = request.getParameter("text");
            String documentContent  = request.getParameter("documentContent");
            String categoryName     = request.getParameter("categoryName");

            if(clientId==null)      {
                logger.warn(LNAME+"PostRoutine.ClientId is null");
            }
            if(text==null)          {
                logger.warn(LNAME+"PostRoutine.Text is null");
            }
            if(categoryName==null)  {
                logger.warn(LNAME+"PostRoutine.CategoryName is null");
            }

            User user=null;
            try {
                user = blogService.getUserByClientId(clientId);

            }catch(RepositoryException e) {
                logger.error(e.getMessage());
            }

            if(user==null){
                logger.warn(LNAME+"PostRoutine.User not found.ClientId:"+clientId);
            }

            if(user.getUserLoginInfo()==null){
                logger.error(LNAME+"PostRoutine.User doesn't have userlogininfo.ClientId:"+clientId);
            }

            String username=user.getUserLoginInfo().getUsername();
            Document document=null;

            if(documentContent!=null && documentContent.length()>0) {
                document = new Document();
                document.setContent(documentContent.getBytes());
                document.setStatus(DocumentStatus.APPROVED);
                document.setType(DocumentType.JPG);
            }

            Post post= new Post();
            post.setText(text);
            try{
                blogService.addPost(post, categoryName, username, document);

            }catch (CategoryNotFoundException e) {
                logger.error(e.getMessage());
            }catch (UserNotFoundException e){
                logger.error(e.getMessage());
            }catch (RepositoryException e){
                logger.error(e.getMessage());
            }
    }

    public void CategoryRoutine(HttpServletRequest request,HttpServletResponse response)
    {
        String clientId = request.getParameter("id");
        String name = request.getParameter("name");

        if(clientId==null) logger.warn("CategoryRoutine - NULL CLIENT ID");

        if(name == null)   logger.warn("CategoryRoutine - NULL NAME");

        Category category=null;

        try {
            category = blogService.getCategoryByName(name);
        }catch (RepositoryException e){
            logger.error(e.getMessage());
        }

        if (category == null) {

            Category item= new Category();
            item.setName(name);

            try {
                blogService.addCategory(item);
            }catch (RepositoryException e){
                logger.error(e.getMessage());
            }
        }
     }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String type = request.getParameter("type");

            switch (type) {
                case "HEARTBEAT":
                    HeartBeatRoutine(request, response);
                    break;
                case "POST":
                    PostRoutine(request, response);
                default:
                case "CATEGORY":
                    CategoryRoutine(request, response);
                    break;
            }
        }catch (Exception e){
            logger.warn("Exception in doPost method");
        }
    }
}
