package com.example.connection;

import com.example.entities.Client;
import com.example.entities.User;
import com.example.exception.ClientNotFoundException;
import com.example.exception.RepositoryException;
import com.example.exception.UserNotFoundException;
import com.example.service.BlogServiceRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


@Singleton
public class ClientManager implements ClientManagerRemote {

    @EJB
    BlogServiceRemote blogService;
    private Logger logger = LoggerFactory.getLogger(ClientManager.class);
    private List<Client> connectedClients;

    @Override
    public void addClient(String clientId, String port, String username, String role) throws UserNotFoundException{

            if (clientExists(clientId)) {
                    updateClient(clientId,Integer.parseInt(port),username,role, true);
            }else{

                Client client = new Client();
                client.setId(clientId);
                client.setClientRole(role);
                client.setPort(Integer.parseInt(port));
                client.setStatus(true);
                client.setLatestActivity(new Date());
                client.setLatestMessage(new Date());

                User user=null;

                try {
                    user = blogService.getUserByUsername(username);
                }catch (RepositoryException e){
                    logger.error(e.getMessage());
                }

                if (user == null) throw new UserNotFoundException("User with username:" + username + " not found");

                connectedClients.add(client);

                try {
                    blogService.addClient(client, user);
                }catch (RepositoryException e){
                    logger.error(e.getMessage());
                }

                return;
            }
    }

    @Override
    public boolean clientExists(String clientId){
        try {

            if(connectedClients==null || connectedClients.size()==0){
                return false;
            }

            for (int index = 0; index < connectedClients.size(); index++) {

                Client connectedClient = connectedClients.get(index);
                if(connectedClient==null){
                    logger.error("Null client - Id:"+clientId);
                }
                if (connectedClient.getId().equals(clientId)) return true;
            }

        }catch (ArrayIndexOutOfBoundsException e) {
            logger.error(e.getMessage());
        }catch (Exception e){
            logger.error("Exception while iterating connected clients");
        }

        return false;
    }

    @Override
    public Client getClientById(String clientId){

        try {
            if(connectedClients==null || connectedClients.size()==0) return null;

            for (int index = 0; index < connectedClients.size(); index++) {
                Client connectedClient = connectedClients.get(index);
                if (connectedClient == null) {
                    logger.error("Null client - Id:" + clientId);
                }

                if (connectedClient.getId().equals(clientId)) return connectedClient;
            }

        }catch (ArrayIndexOutOfBoundsException e){
            logger.error(e.getMessage());
        }catch (Exception e){
            logger.error("General exception while getting client by id");
        }
        return null;
    }

    @Override
    public int getClientIndex(String clientId){
        try {
                if(connectedClients==null || connectedClients.size()==0) return -1;

                for (int index = 0; index < connectedClients.size(); index++) {
                    Client connectedClient = connectedClients.get(index);

                    if(connectedClient==null){
                        logger.error("Null client - Id: "+clientId);
                    }
                    if (connectedClient.getId().equals(clientId)) return index;
                }

        }catch (ArrayIndexOutOfBoundsException e){
            logger.error("ArrayIndexOutOfBounds - ClientId:"+clientId);
        }catch (Exception e){
            logger.error("General exception");
        }

        return -1;

    }

    @Override
    public List<Client> getConnectedClients() {
        return this.connectedClients;
    }

    @Override
    public void setConnectedClients(List<Client> list) {
        connectedClients = list;
    }

    @PostConstruct
    public void init() {
        connectedClients = new ArrayList<Client>();
    }

    @Override
    public boolean isClientListEmpty() {
        return (connectedClients.size() == 0);
    }

    /**
     * <p>Removes clients from connectedClients list and from CLIENT table</p>
     *
     * @param clientIds List of clientIds to be removed
     */
    @Override
    public void removeClients(List<String> clientIds){
            if(connectedClients==null || connectedClients.size()==0) return;

            List<Integer> removed=new ArrayList<Integer>();
            try {
                for (Iterator<Client> iterator = connectedClients.iterator(); iterator.hasNext(); ) {
                    Client client = iterator.next();
                    if (clientIds.contains(client.getId())) {
                        iterator.remove();
                    }
                }
            }catch (Exception e){
                logger.error("Exception in client iterator while removing clients");
            }

            for (String clientId : clientIds) {
                try {
                    blogService.removeClient(clientId);
                }catch (RepositoryException e){
                    logger.error("Exception while removing clients"+e.getMessage());
                }
            }

    }

    @Override
    public void updateClient(Client client){

        Client updated=getClientById(client.getId());

        updated.setLatestActivity(new Date());
        int index=getClientIndex(client.getId());

        connectedClients.set(index,updated);

        try {
            blogService.updateClient(client.getId(), updated);

        }catch (RepositoryException e){
            logger.error("Repository exception while updating client:"+e.getMessage());
        }
    }


    @Override
    public void updateClient(String clientId, int port, String username, String role,boolean alive){

        Client client=getClientById(clientId);

        if(alive){
            client.setLatestMessage(new Date());
        }else{
            client.setLatestActivity(new Date());
        }

        int index=getClientIndex(clientId);
        connectedClients.set(index,client);

        try {
            blogService.updateClient(clientId,port,username,role, alive);
        }catch (ClientNotFoundException e){
            logger.error("Exception while updating client"+e.getMessage());
        }catch (RepositoryException e){
            logger.error("Exception while updating client:"+e.getMessage());
        }
    }
}
