package com.example.connection;

import com.example.entities.Client;
import com.example.exception.UserNotFoundException;

import javax.ejb.Remote;
import java.util.List;

/**
 * Created by sonat on 25.08.2016.
 */

@Remote
public interface ClientManagerRemote {

    void addClient(String clientId, String port, String username, String role) throws UserNotFoundException;

    boolean clientExists(String clientId);

    Client getClientById(String clientId);

    int getClientIndex(String clientId);

    List<Client> getConnectedClients();

    void setConnectedClients(List<Client> list);

    boolean isClientListEmpty();

    void removeClients(List<String> clientIds);

    void updateClient(Client client);

    void updateClient(String clientId, int port, String username, String role,boolean alive);
}
