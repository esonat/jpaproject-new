package com.example.connection;

/**
 * Created by sonat on 14.08.2016.
 */
public class HeartBeatMessage {
    private String clientId;
    private String messageType;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
}