package com.example.startup;

import javax.ejb.Local;

/**
 * Created by sonat on 19.08.2016.
 */

@Local
public interface ApplicationManagerLocal {
    void startup();
    void shutdown();
}
