package com.example.startup;


import com.example.entities.Task;
import com.example.entities.TaskCategory;
import com.example.entities.TaskType;
import com.example.task.TaskManagerRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import java.util.UUID;

@ApplicationScoped
public class ApplicationManager{
    Logger logger= LoggerFactory.getLogger(ApplicationManager.class);
    private static String LNAME="APPLICATIONMANAGER";

    @EJB
    TaskManagerRemote taskManager;

    public void init(@Observes @Initialized(ApplicationScoped.class) Object init){
        try{

            String[] list=new String[]{TaskType.TASK_ASSIGNROLE.toString(),TaskType.TASK_REMOVEDISCONNECTED.toString()};
            String category= TaskCategory.CLIENTTASK.toString();

            for(int index=0;index<list.length;index++){
                String taskType=list[index];

                if(taskType==null) {
                    logger.error(LNAME+" TaskType:"+taskType+"is null");
                    return;
                }

                Task task=new Task();
                task.setTaskId(UUID.randomUUID());
                task.setCategory(category);
                task.setType(taskType);
                task.setLongRunning(true);

                taskManager.addTask(task);
            }

        }catch (ArrayIndexOutOfBoundsException e){
            logger.error("Exception while initializing Application Manager"+e.getMessage());
        } catch(Exception e){
            logger.error("Exception while initializing Application Manager");
        }
    }

    public void destroy(@Observes @Destroyed(ApplicationScoped.class) Object init){

    }

}
