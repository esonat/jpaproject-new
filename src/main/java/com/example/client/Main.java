package com.example.client;

import com.example.entities.Phone;
import com.example.service.BlogServiceRemote;

import javax.naming.InitialContext;

/**
 * Created by sonat on 15.09.2016.
 */
public class Main {

    public static void main(String[] args) throws Exception
    {
//        Hashtable env = new Hashtable();
//        env.put(Context.INITIAL_CONTEXT_FACTORY,
//                "com.sun.enterprise.naming.SerialInitContextFactory");
//        Properties p = new Properties();
// optional. Defaults to localhost. Only needed if web server is running
// on a different host than the appserver
//        p.setProperty(Context.INITIAL_CONTEXT_FACTORY,"com.sun.enterprise.naming.SerialInitContextFactory");
//        p.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
//        p.setProperty("org.omg.CORBA.ORBInitialPort", "3700");
//        Context context = new InitialContext(p);


//        Properties props = new Properties();
//        props.setProperty(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.cosnaming.CNCtxFactory");
//        props.setProperty(Context.PROVIDER_URL,System.getProperty(Context.PROVIDER_URL));

//        Context context = new InitialContext(props);
        ///BlogService blogService = (BlogService)new InitialContext().lookup("java:global/Blog/BlogService");
        //InitialContext context = new InitialContext(props);
        //BlogService blogService = (BlogService)context.lookup("java:global/Blog/BlogService");

//        Properties props = new Properties();
//
//        props.setProperty(Context.INITIAL_CONTEXT_FACTORY,"com.sun.enterprise.naming.impl.SerialInitContextFactory");
//        props.setProperty(Context.URL_PKG_PREFIXES="com.sun.enterprise.naming");
//
//
//        java.naming.factory.initial=com.sun.enterprise.naming.impl.SerialInitContextFactory
//        java.naming.factory.url.pkgs=com.sun.enterprise.naming
//        java.naming.factory.state=com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl
////
//        props.setProperty("java.naming.factory.initial",
//                "com.sun.enterprise.naming.SerialInitContextFactory");
//
//        props.setProperty("java.naming.factory.url.pkgs",
//                "com.sun.enterprise.naming");
//
//        props.setProperty("java.naming.factory.state",
//                "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
        InitialContext context=new InitialContext();
        BlogServiceRemote blogService=(BlogServiceRemote)context.lookup("java:global/Blog/BlogService!com.example.service.BlogServiceRemote");



//        System.out.println(blogService.findPhoneByNumber("1234").getUser().getId());


        for(Phone phone:blogService.getAllPhones()){
            System.out.println(phone.getNum());
        }

//    try {
        // database connection
//        Class driverClass = Class.forName("com.mysql.cj.jdbc.Driver");
//        Connection jdbcConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/JPAProject?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "sonat");
//        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
//
//        // write DTD file
//        FlatDtdDataSet.write(connection.createDataSet(), new FileOutputStream("dataset.dtd"));

        // database connection
        //ResourceCache resourceCache = ResourceCache.getInstance();
//        String driverClassString = "com.mysql.cj.jdbc.Driver";
//        String databaseURL = "jdbc:mysql://localhost:3306/JPAProject?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
//        String username = "root";
//        String password = "sonat";
//
//        Class driverClass = Class.forName(driverClassString);
//        Connection jdbcConnection = DriverManager.getConnection(databaseURL, username, password);
//        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
//        DatabaseConfig config = connection.getConfig();
//        config.setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, true);
//        //config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
//        config.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);
//
//        FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
//        flatXmlDataSetBuilder.setColumnSensing(true);
//
//        IDataSet fullDataSet = connection.createDataSet();
//        FlatXmlDataSet.write(fullDataSet, new FileOutputStream("full.xml"));
//    }catch (SQLSyntaxErrorException e){
//        e.printStackTrace();
//    }catch (Exception e){
//        e.printStackTrace();
//    }
//        IDataSet dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
//                .getContextClassLoader()
//                .getResourceAsStream("full.xml"));



        // database connection
                //com.mysql.cj.jdbc.Driver
//        Class driverClass = Class.forName("com.mysql.cj.jdbc.Driver");
//        Connection jdbcConnection = DriverManager.getConnection(
//                "jdbc:mysql://localhost:3306/JPAProject?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "sonat");
//        String id = "http://www.dbunit.org/features/batchedStatements";
//
//        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection,"JPAProject");
//        DatabaseConfig dBConfig = connection.getConfig(); // dBConn is a IDatabaseConnection
//        dBConfig.setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, true);
//        dBConfig.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES,true);
        //System.out.println(config.getProperty("http://www.dbunit.org/features/caseSensitiveTableNames"));

//        // partial database export
//        QueryDataSet partialDataSet = new QueryDataSet(connection);
//        partialDataSet.addTable("FOO", "SELECT * FROM TABLE WHERE COL='VALUE'");
//        partialDataSet.addTable("BAR");
//        FlatXmlDataSet.write(partialDataSet, new FileOutputStream("partial.xml"));

        // full database export
//        IDataSet fullDataSet = connection.createDataSet();
//        FlatXmlDataSet.write(fullDataSet, new FileOutputStream("full.xml"));

        // dependent tables database export: export table X and all tables that
        // have a PK which is a FK on X, in the right order for insertion
//        String[] depTableNames =
//                TablesDependencyHelper.getAllDependentTables( connection, "X" );
//        IDataSet depDataset = connection.createDataSet( depTableNames );
//        FlatXmlDataSet.write(depDataSet, new FileOutputStream("dependents.xml"));

    }
}
