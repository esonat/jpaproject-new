package com.example.rest.provider;

//@Provider
//public class BlogMapperProvider implements ContextResolver<ObjectMapper>{
//    final ObjectMapper defaultObjectMapper;
//    final ObjectMapper postObjectMapper;
//    final ObjectMapper postBeanObjectMapper;
//
//    public BlogMapperProvider(){
//        defaultObjectMapper=createDefaultMapper();
//        postObjectMapper=createPostObjectMapper();
//        postBeanObjectMapper=createPostBeanObjectMapper();
//    }
//
//    @Override
//    public ObjectMapper getContext(Class<?> type) {
//        System.out.println("getContext(Class<?> type: " + (type == Post.class?"Post":"Default") + ")");
//        if (type == Post.class) {
//            return postObjectMapper;
//        } else if(type == PostBean.class) {
//            return postBeanObjectMapper;
//        }else{
//            return defaultObjectMapper;
//        }
//    }
//
//    private static ObjectMapper createPostObjectMapper(){
//        System.out.println("createPostObjectMapper()");
//        Pair combinedIntrospector = createJaxbJacksonAnnotationIntrospector();
//        ObjectMapper result = new ObjectMapper();
//        result.configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true);
//        result.configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, true);
//        result.setDeserializationConfig(result.getDeserializationConfig().withAnnotationIntrospector(combinedIntrospector));
//        result.setSerializationConfig(result.getSerializationConfig().withAnnotationIntrospector(combinedIntrospector));
//
//        return result;
//    }
//
//    private static ObjectMapper createPostBeanObjectMapper(){
//        System.out.println("createPostBeanObjectMapper()");
//        Pair combinedIntrospector = createJaxbJacksonAnnotationIntrospector();
//        ObjectMapper result = new ObjectMapper();
//        result.configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true);
//        result.configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, true);
//        result.setDeserializationConfig(result.getDeserializationConfig().withAnnotationIntrospector(combinedIntrospector));
//        result.setSerializationConfig(result.getSerializationConfig().withAnnotationIntrospector(combinedIntrospector));
//
//        return result;
//    }
//
//    private static ObjectMapper createDefaultMapper() {
//        System.out.println("createDefaultMapper()");
//        ObjectMapper result = new ObjectMapper();
//        result.configure(Feature.INDENT_OUTPUT, true);
//
//        return result;
//    }
//
//    private static Pair createJaxbJacksonAnnotationIntrospector() {
//        System.out.println("createJaxbJacksonAnnotationIntrospector()");
//        AnnotationIntrospector jaxbIntrospector = new JaxbAnnotationIntrospector();
//        AnnotationIntrospector jacksonIntrospector = new JacksonAnnotationIntrospector();
//
//        return new AnnotationIntrospector.Pair(jacksonIntrospector, jaxbIntrospector);
//    }
//
//}
