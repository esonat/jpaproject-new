package com.example.rest.modelprocessor;

import org.glassfish.jersey.process.Inflector;
import org.glassfish.jersey.server.model.ModelProcessor;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceModel;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

/**
 * Created by sonat on 03.09.2016.
 */

@Provider
public class VersionsModelProcessor implements ModelProcessor{
    @Override
    public ResourceModel processResourceModel(ResourceModel resourceModel,Configuration configuration){
        ResourceModel.Builder newResourceModelBuilder=new ResourceModel.Builder(false);

        for(final Resource resource:resourceModel.getResources()){
            final Resource.Builder resourceBuilder=Resource.builder(resource);
            resourceBuilder.addChildResource("version")
                    .addMethod(HttpMethod.GET)
                    .handledBy(new Inflector<ContainerRequestContext,String>(){
                        @Override
                        public String apply(ContainerRequestContext cr){
                            return "version : 1.0";
                        }
                    }).produces(MediaType.TEXT_PLAIN).extended(true);

            newResourceModelBuilder.addResource(resourceBuilder.build());
        }

        final ResourceModel newResourceModel=newResourceModelBuilder.build();
        return newResourceModel;
    }

    @Override
    public ResourceModel processSubResource(ResourceModel subResourceModel,Configuration configuration){
        return subResourceModel;
    }
}
