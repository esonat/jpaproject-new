package com.example.rest.entityprovider;

import com.example.entities.Post;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Provider
@Produces("application/csv")
public class CSVMessageBodyWriter implements MessageBodyWriter<List<Post>> {

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return (List.class.isAssignableFrom(type));
    }

    @Override
    public long getSize(List<Post> posts, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    @Override
    public void writeTo(List<Post> dataList, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        ICsvBeanWriter writer=new CsvBeanWriter(new PrintWriter(entityStream),
                CsvPreference.STANDARD_PREFERENCE);

        if(dataList==null || dataList.size()==0){
            return;
        }

        String[] nameMapping=getHeader(dataList.get(0));
        writer.writeHeader(nameMapping);

        for(Object p:dataList){
            writer.write(p,nameMapping);
        }

        writer.close();
    }
    private String[] getHeader(Object obj) {
        ArrayList<String> headers = new ArrayList<String>();

        try {
            BeanInfo info = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] pds = info.getPropertyDescriptors();
            for (PropertyDescriptor pd : pds) {
                //Skip getClass() property
                if (!pd.getName().equals("class")) {
                    headers.add(pd.getDisplayName());
                }
             }

        } catch (IntrospectionException ex) {
            Logger.getLogger(CSVMessageBodyWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return headers.toArray(new String[headers.size()]);
    }
}
