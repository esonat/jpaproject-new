//package com.example.rest.interceptor;
//
//import javax.ws.rs.WebApplicationException;
//import javax.ws.rs.core.MultivaluedMap;
//import javax.ws.rs.ext.Provider;
//import javax.ws.rs.ext.WriterInterceptorContext;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.util.zip.GZIPOutputStream;
//
///**
// * Created by sonat on 03.09.2016.
// */
//
//@Provider
//public class BlogWriterInterceptor implements javax.ws.rs.ext.WriterInterceptor{
//    @Override
//    public void aroundWriteTo(WriterInterceptorContext context)
//        throws IOException,WebApplicationException{
//        MultivaluedMap<String,Object> headers=context.getHeaders();
//        headers.add("Content-Encoding","gzip");
//        OutputStream outputStream=context.getOutputStream();
//        context.setOutputStream(new GZIPOutputStream(outputStream));
//
//        context.proceed();
//    }
//}
