package com.example.rest.exception;

import com.example.exception.CategoryNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by sonat on 17.09.2016.
 */

@Provider
public class CategoryNotFoundExceptionMapper implements ExceptionMapper<CategoryNotFoundException> {

    @Override
    public Response toResponse(CategoryNotFoundException exception){
        return Response.status(Response.Status.NOT_FOUND)
                .entity("Category not found").build();
    }

}
