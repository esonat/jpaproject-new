package com.example.rest.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.io.IOException;


@Provider
public class IOExceptionMapper implements ExceptionMapper<IOException>{
    @Override
    public Response toResponse(IOException exception){
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("IO Exception:"+exception.getMessage()).build();
    }
}
