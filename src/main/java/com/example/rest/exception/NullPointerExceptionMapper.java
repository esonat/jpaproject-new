package com.example.rest.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class NullPointerExceptionMapper implements ExceptionMapper<NullPointerException>{
    @Override
    public Response toResponse(NullPointerException exception){
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("Null pointer exception:"+exception.getMessage()).build();
    }
}
