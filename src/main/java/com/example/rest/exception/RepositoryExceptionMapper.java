package com.example.rest.exception;

import com.example.exception.RepositoryException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RepositoryExceptionMapper implements ExceptionMapper<RepositoryException> {

    @Override
    public Response toResponse(RepositoryException exception){
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("Repository exception:"+exception.getMessage()).build();
    }
}
