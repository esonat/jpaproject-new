package com.example.rest.exception;

import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by sonat on 17.09.2016.
 */
@Provider
public class PersistenceExceptionMapper implements ExceptionMapper<PersistenceException> {
    @Override
    public Response toResponse(PersistenceException exception){
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("Persistence Exception:"+exception.getMessage()).build();
    }
}
