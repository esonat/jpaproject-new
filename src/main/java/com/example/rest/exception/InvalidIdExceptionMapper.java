package com.example.rest.exception;

import com.example.exception.InvalidIdException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class InvalidIdExceptionMapper implements ExceptionMapper<InvalidIdException> {

    @Override
    public Response toResponse(InvalidIdException exception){
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity("Invalid id").build();
    }
}
