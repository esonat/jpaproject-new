package com.example.rest.exception;

import com.example.exception.PostNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class PostNotFoundExceptionMapper implements ExceptionMapper<PostNotFoundException> {

    @Override
    public Response toResponse(PostNotFoundException exception){
        return Response.status(Response.Status.NOT_FOUND)
                .entity("Post not found").build();
    }
}
