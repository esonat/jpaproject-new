package com.example.rest.monitor;

import org.glassfish.jersey.server.spi.Container;
import org.glassfish.jersey.server.spi.ContainerLifecycleListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.ws.rs.ext.Provider;


@Singleton
@Provider
public class ContainerLifecycleListenerImpl implements ContainerLifecycleListener{

    private static final Logger logger = LoggerFactory.getLogger(ContainerLifecycleListenerImpl.class.getName());

    @Override
    public void onStartup(Container container){
        logger.info("ContainerLifecycleListenerImpl::onStartup");
    }

    @Override
    public void onReload(Container container){
        logger.info("ContainerLifecycleListenerImpl::onReload");
    }

    @Override
    public void onShutdown(Container container){
        logger.info("ContainerLifecycleListenerImpl::onShutdown");
    }

}
