package com.example.rest.monitor;

import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;

import javax.ws.rs.ext.Provider;


@Provider
public class RequestEventListenerImpl implements RequestEventListener {

    @Override
    public void onEvent(RequestEvent event){
        switch (event.getType()){
            case RESOURCE_METHOD_START:
                System.out.println("Resource method "+
                event.getUriInfo().getMatchedResourceMethod()
                        .getHttpMethod()
                        + " started for request");
                break;
            case FINISHED:
                System.out.println("Request "+"finished");
                break;
        }
    }
}
