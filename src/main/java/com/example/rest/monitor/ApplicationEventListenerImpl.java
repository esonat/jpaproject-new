package com.example.rest.monitor;


import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ext.Provider;

@Provider
public class ApplicationEventListenerImpl implements ApplicationEventListener{


    private static final Logger logger = LoggerFactory.getLogger(ApplicationEventListenerImpl.class.getName());

    @Override
    public void onEvent(ApplicationEvent event){
        logger.info("ApplicationEventListenerImpl:onEvent");
        switch (event.getType()){
            case INITIALIZATION_START:
                logger.info("INITIALIZATION_START");
                break;
            case INITIALIZATION_APP_FINISHED:
                logger.info("INITIALIZATION_APP_FINISHED");
                break;
            case INITIALIZATION_FINISHED:
                logger.info("INITIALIZATION_FINISHED");
                break;
            case RELOAD_FINISHED:
                logger.info("RELOAD_FINISHED");
                break;
            case DESTROY_FINISHED:
                logger.info("DESTROY_FINISHED");
                break;
        }
    }

    @Override
    public RequestEventListener onRequest(RequestEvent re){
        logger.info("ApplicationEventListenerImpl:onRequest");
        return new RequestEventListenerImpl();
    }
}
