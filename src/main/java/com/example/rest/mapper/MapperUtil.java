package com.example.rest.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by sonat on 17.09.2016.
 */
public class MapperUtil {

    public static String getJsonString(Object object) throws JsonProcessingException{
        ObjectMapper mapper = new ObjectMapper();
        String indented=mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);

        return indented;
    }
}
