//package com.example.rest.client;
//
//import com.example.entities.Post;
//import com.example.rest.bean.PostBean;
//import com.example.rest.filter.ClientAuthenticationFilter;
//import com.example.rest.filter.LogErrorResponseFilter;
//import com.example.rest.interceptor.BlogReaderInterceptor;
//import com.example.rest.interceptor.BlogWriterInterceptor;
//
//import javax.ws.rs.client.Client;
//import javax.ws.rs.client.ClientBuilder;
//import javax.ws.rs.client.Entity;
//import javax.ws.rs.client.WebTarget;
//import javax.ws.rs.core.MediaType;
//
//import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
//
//
//public class RestClient {
//
//
//    public static void SendLogToSlack(String log){
//
//        Client client= ClientBuilder.newClient();
//        WebTarget webTarget= client.target("https://hooks.slack.com/services/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");
//
//        String json="{\"text\":\""+log+"\"}";
//        webTarget.request(APPLICATION_JSON)
//                .post(Entity.entity(json,APPLICATION_JSON));
//    }
//
//    public static void BasicAuthentication(){
//        //final String BASE_URI="http://localhost:8080/Blog/services/post/32";
//        Client client=ClientBuilder.newClient();
//        WebTarget webTarget=client.target("http://localhost:8080/Blog/services/post/32");
//
//        String username="engin";
//        String password="sonat";
//
//        ClientAuthenticationFilter filter=new ClientAuthenticationFilter(username,password);
//        webTarget.register(filter);
//
//        Post post=webTarget.request(APPLICATION_JSON).get(Post.class);
//
//        System.out.println(post.getText());
//        client.close();
//
//    }
//
//    public static void main(String[] args){
//
//       // BasicAuthentication();
//        SendLogToSlack("deneme");
////        try {
////            new RestClient().SendClientHeader();
////
////        }catch (Exception e){
////            System.out.print(e.getMessage());
////        }
//    }
//
//    public void SendPostList(){
//        String BASE_URI = "http://localhost:8080/Blog/services";
//            Client client = ClientBuilder.newClient();
//            WebTarget webTarget = client.target(BASE_URI).path("post");
//
//            for (int i = 0; i < 1000; i++) {
//                PostBean postBean = new PostBean();
//                postBean.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris elit elit, vestibulum at neque nec, hendrerit vulputate mi. Cras facilisis aliquam neque, ut porta dui feugiat a. Donec dictum eget sem ac rhoncus. Mauris semper est nec nisi feugiat elementum. Fusce et justo sed lacus aliquet condimentum. Cras sollicitudin ipsum in orci sodales, nec ullamcorper massa dapibus. Donec ultricies imperdiet mollis. In vel mi eget nibh laoreet fringilla. Aliquam sit amet enim tristique nibh suscipit suscipit nec at metus. Duis bibendum nulla in ante egestas malesuada in eu tortor. Curabitur leo erat, porttitor ac leo lacinia, feugiat condimentum est.\n" +
//                        "\n" +
//                        "Vivamus ac dignissim diam. Ut ac odio vel dolor suscipit dictum at quis massa. Sed vitae ipsum condimentum, pellentesque quam a, congue augue. Curabitur quis lectus nec diam maximus faucibus ac ac turpis. Nullam arcu nunc, laoreet ut blandit vel, sagittis vitae libero. Mauris dui nibh, finibus ut diam nec, auctor congue dolor. Nulla non quam quis odio efficitur cursus et ut erat. Mauris varius gravida rutrum. Praesent vehicula urna a est cursus ullamcorper. Morbi et orci id justo ultricies venenatis ac ut ante.\n" +
//                        "\n" +
//                        "Donec turpis augue, semper quis luctus id, eleifend eu orci. Nulla ut volutpat velit, condimentum ultrices lorem. Vestibulum posuere sapien massa, sed dapibus dui dapibus non. Pellentesque sit amet vulputate urna. Vivamus pretium est non felis vulputate, ac suscipit felis lacinia. Cras finibus, purus et tristique luctus, orci velit dapibus est, id tempor nulla elit efficitur massa. Suspendisse auctor eleifend eros et congue. Maecenas id augue sed ipsum venenatis cursus. In condimentum ac arcu eget tempus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum nunc mi, hendrerit nec pellentesque sed, maximus ac libero. Integer sit amet massa ac lorem molestie lobortis. Nulla in mattis felis, non pellentesque arcu. Proin est leo, consectetur id est non, sagittis faucibus libero. Cras ante lorem, volutpat et sem nec, vehicula fermentum mauris. Integer consectetur tristique aliquet.\n" +
//                        "\n");
//                postBean.setCategory("Java");
//                postBean.setUsername("engin");
//
//                webTarget.request(APPLICATION_JSON)
//                        .post(javax.ws.rs.client.Entity.entity(postBean, APPLICATION_JSON));
//            }
////            AsyncInvoker asyncInvoker = webTarget.request(APPLICATION_JSON).async();
////
////            Future<Response> responseFuture = asyncInvoker.get();
////            Response response = responseFuture.get();
////
////            List<Post> posts=response.readEntity(new GenericType<List<Post>>(){});
////
////            for(Post post:posts){
////                System.out.println(post.getText());
////            }
//    }
//
//    public void SendClientHeader(){
//        String BASE_URI = "http://localhost:8080/Blog/services/post/32";
//
//        Client client = ClientBuilder.newClient()
//        .register(LogErrorResponseFilter.class)
//        .register(BlogReaderInterceptor.class)
//        .register(BlogWriterInterceptor.class);
//
//
////        .register(ClientHeaderFilter.class)
//
//        WebTarget target=client.target(BASE_URI);
//
//        Post post=target.request(MediaType.APPLICATION_JSON_TYPE)
//                .header("JPAProject","JPA")
//                .header("Content-Encoding","gzip").get(Post.class);
//
//        System.out.println(post.getText());
//    }
//}
