package com.example.rest.resource;

import com.example.entities.*;
import com.example.exception.DuplicatePhoneException;
import com.example.exception.DuplicateUsernameException;
import com.example.exception.RepositoryException;
import com.example.rest.bean.UserBean;
import com.example.service.BlogServiceRemote;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Path("/user")
public class UserResource {
    @EJB
    BlogServiceRemote blogService;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserById(@PathParam("id")Integer id){
        //if(id==null) return Response.status(404).build();

        if(id<0)
            return Response.status(500).entity("User id cannot be smaller than 0").build();

        User user;

        try {
            user = blogService.findUserById(id);

            if (user == null)
                return Response.status(404).entity("User not found").build();

        }catch (Exception e){
            return Response.status(500).entity("Exception").build();
        }

        return Response.ok(user).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addUser(UserBean userBean){
    try {
        ValidatorFactory factory= Validation.buildDefaultValidatorFactory();
        Validator validator=factory.getValidator();

        Set<ConstraintViolation<UserBean>> violations=validator.validate(userBean);
        String validateMessage="";

        for(ConstraintViolation<UserBean> violation:violations){
            validateMessage+=violation.getMessage();
        }

        if(!validateMessage.equals("")) return Response.status(500).entity(validateMessage).build();

        List<String> roles = new ArrayList<String>();
        List<Phone> phones = new ArrayList<Phone>();

        roles.add("Admin");
        roles.add("PostUser");
        roles.add("CategoryUser");

        Phone phone1 = new Phone();
        phone1.setNum(userBean.getPhone());
        phone1.setType("Work");
        phones.add(phone1);

        City cityItem;
        try {
            cityItem = blogService.getCityByName(userBean.getCity());
        } catch (Exception e) {
            return Response.status(500).entity("Exception:"+e.getMessage()).build();
        }
        if(cityItem==null) return Response.status(500).entity("City not found").build();

        Address address = new Address();
        address.setCity(cityItem);
        address.setZip(userBean.getZip());
        address.setStreet(userBean.getStreet());
        address.setState(userBean.getState());

        UserInfo userInfo = new UserInfo();
        userInfo.setName(userBean.getName());
        userInfo.setAddress(address);
        userInfo.setPhones(phones);

        UserLoginInfo userLoginInfo = new UserLoginInfo();
        userLoginInfo.setUsername(userBean.getUsername());
        userLoginInfo.setPassword(userBean.getPassword());

        User user = new User();
        user.setUserinfo(userInfo);
        user.setUserLoginInfo(userLoginInfo);

        try {
            blogService.addUser(user, roles, phones);
        }catch (DuplicateUsernameException e){
            return Response.status(500).entity("Duplicate username").build();
        }catch (DuplicatePhoneException e) {
            return Response.status(500).entity("Duplicate phone").build();
        }catch (EJBException e){
            return Response.status(500).entity("EJBException:"+e.getMessage()).build();
        }catch (PersistenceException e){
            return Response.status(500).entity("Persistence exception:"+e.getMessage()).build();
        }catch (Exception e){
            return Response.status(500).entity("Exception:"+e.getMessage()).build();
        }

        return Response.ok("User added").build();
    }catch (Exception e){
        return Response.status(500).entity(e.getMessage()).build();
    }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsers(@DefaultValue("0")@QueryParam("start") int start,
                                @DefaultValue("0")@QueryParam("limit") int limit)
  throws RepositoryException {

            List<User> users = blogService.getAllUsers();
            List<User> list = new ArrayList<User>();
            //String response = MapperUtil.getJsonString(users);

            if (limit == 0 && start == 0) return Response.status(200).entity(users).build();

            if (limit < 0) return Response.status(500).entity("Query limit cannot be less than 0").build();
            if (start >= users.size()) return Response.status(500).entity("Invalid start parameter").build();

            if (start < 0) start = 0;
            if (start + limit > users.size()) limit = users.size() - start;

            for (int index = start; index < start + limit; index++) {
                list.add(users.get(index));
            }
            //response = MapperUtil.getJsonString(list);
            return Response.status(200).entity(list).build();
    }
}
