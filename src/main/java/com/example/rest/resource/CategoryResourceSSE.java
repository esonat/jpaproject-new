package com.example.rest.resource;

import com.example.entities.Category;
import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseBroadcaster;
import org.glassfish.jersey.media.sse.SseFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by sonat on 04.09.2016.
 */

@Path("/user")
@Singleton
public class CategoryResourceSSE {

    private static ArrayList<String> modifiedUsers=new ArrayList<String>();
    private static final SseBroadcaster broadcaster=new SseBroadcaster();
    private static final Logger logger= LoggerFactory.getLogger(CategoryResourceSSE.class);


    @GET
    @Produces(SseFeature.SERVER_SENT_EVENTS)
    public EventOutput manageSSEEvents(@HeaderParam(SseFeature.LAST_EVENT_ID_HEADER)
                                       @DefaultValue("-1")int lastEventId){

        EventOutput eventOutput=new EventOutput();
        if(lastEventId>0){
            replayMissedUpdates(lastEventId,eventOutput);
        }
        if(!broadcaster.add(eventOutput)){
            throw new ServiceUnavailableException(5L);
        }
        return eventOutput;
    }

    private void replayMissedUpdates(final int lastEventId,final EventOutput eventOutput){
        try{
            for(int i=lastEventId;i<modifiedUsers.size();i++){
                eventOutput.write(createItemEvent(i,modifiedUsers.get(i)));
            }

        }catch (IOException ex){
            throw new InternalServerErrorException("Error replaying missed events",ex);
        }
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id")int id,Category entity){
        final int modifiedListIndex=modifiedUsers.size()+1;
        broadcaster.broadcast(createItemEvent(modifiedListIndex,entity.getName()));
    }

    private OutboundEvent createItemEvent(final int eventId,final String name){
        return new OutboundEvent.Builder()
                .id(String.valueOf(eventId))
                .data(String.class,name).build();
    }
}
