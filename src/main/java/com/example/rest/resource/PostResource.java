package com.example.rest.resource;

import com.example.entities.*;
import com.example.exception.*;
import com.example.rest.bean.PostBean;
import com.example.rest.filter.RequestLogger;
import com.example.rest.mapper.MapperUtil;
import com.example.service.BlogServiceRemote;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by sonat on 01.09.2016.
 */
@Path("/post")
public class PostResource {
    Logger logger= LoggerFactory.getLogger(PostResource.class);

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @EJB
    BlogServiceRemote blogService;

    @Context
    private HttpServletRequest request;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @RequestLogger
    public Response findPostById(@PathParam("id")int id) throws InvalidIdException,
            PostNotFoundException,
            EJBException,
            PersistenceException,
            RepositoryException{

        if(id<0) throw new InvalidIdException();

            Post post = blogService.getPostById(id);

            if (post == null) throw new PostNotFoundException();

            return Response.status(200).entity(post).build();

        //return Response.ok().links(getLinks(id))
          //      .entity(post).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllPosts(@DefaultValue("0") @QueryParam("start") int start,
                                 @DefaultValue("0") @QueryParam("length") int length)
            throws RepositoryException,JsonProcessingException{

            List<Post> posts = blogService.getAllPosts();
            List<Post> postList=new ArrayList<Post>();

            String jsonResponse= MapperUtil.getJsonString(posts);

            if(start==0 && length==0) {
                return Response.status(200).entity(jsonResponse).build();
            }

            if(start<0) return Response.status(500).entity("Start parameter cannot be negative").build();
            if(length<0) return Response.status(500).entity("Length parameter cannot be negative").build();

            if(start>posts.size()) start=posts.size()-1;
            if(start+length>posts.size()) length=posts.size()-start;


            for(int index=start;index<start+length;index++){
                postList.add(posts.get(index));
            }

            jsonResponse=MapperUtil.getJsonString(postList);
            Response.ResponseBuilder response=Response.status(200).entity(jsonResponse);

            CacheControl cc=new CacheControl();
            cc.setMaxAge(1000);
            cc.setPrivate(false);

            response.cacheControl(cc);
            return response.build();
    }

    private Link[] getLinks(int postId){
        Link userLink=Link.fromUri("{id}/user")
                .rel("user").build(postId);

        return new Link[]{userLink};
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPost(PostBean postBean,@Context UriInfo uriInfo)
    throws  RepositoryException,
            CategoryNotFoundException,
            NullPointerException,
            UserNotFoundException
    {
        ValidatorFactory factory= Validation.buildDefaultValidatorFactory();
        Validator validator=factory.getValidator();

        Set<ConstraintViolation<PostBean>> violations=validator.validate(postBean);
        String validateMessage="";

        for(ConstraintViolation<PostBean> violation:violations){
            validateMessage+=violation.getMessage();
        }

        if(!validateMessage.equals("")){
            return Response.status(500).entity(validateMessage).build();
        }

        String text     =   postBean.getText();
        String username =   postBean.getUsername();
        String category =   postBean.getCategory();

        Post post = new Post();
        post.setText(text);

        int ID = blogService.addPost(post, category, username, null);

        UriBuilder builder=uriInfo.getAbsolutePathBuilder();
        builder.path(String.valueOf(ID));

        //RETURN RESPONSE
        //return Response.status(200).entity("Post added").build();
        return Response.created(builder.build()).build();
    }


    @POST
    @Path("{id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public void addPostWithDocument(@FormDataParam("document") InputStream in,
                                    @FormDataParam("document")FormDataContentDisposition fileDetail,
                                    @PathParam("id")int id) throws  IOException,
                                                                    RepositoryException ,
                                                                    PostNotFoundException{

        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        int size=request.getContentLength();
        byte[] buffer=new byte[size];
        int len=0;

        while((len=in.read(buffer,0,10240))!=-1){
            bos.write(buffer,0,len);
        }
        byte[] imgBytes=bos.toByteArray();

        Document document=new Document();
        document.setContent(imgBytes);
        document.setType(DocumentType.JPG);
        document.setStatus(DocumentStatus.APPROVED);

        blogService.updatePost(id,document);
    }


    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePost(@PathParam("id")int id,PostBean postBean) throws EJBException,
        PersistenceException,
        RepositoryException,
        UserNotFoundException,
        CategoryNotFoundException,
        PostNotFoundException,
        InvalidIdException
    {
        if(id<1) throw new InvalidIdException();

            blogService.updatePost(id,
                    postBean.getText(),
                    postBean.getUsername(),
                    postBean.getCategory());

//        }catch (EJBException e){
//            throw new EJBException(e);
//        }catch (PersistenceException e){
//            throw new PersistenceException(e);
//        }catch (RepositoryException e) {
//            throw new RepositoryException(e);
//        }catch (UserNotFoundException e){
//            throw new UserNotFoundException();
//        }catch (CategoryNotFoundException e){
//            throw new CategoryNotFoundException();
//        }catch (PostNotFoundException e){
//            throw new PostNotFoundException();
//        }

        return Response.status(200).entity("Post updated").build();
    }




    @GET
    @Path("{id}/category")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategoryOfPost(@PathParam("id")int id) throws
        InvalidIdException,
        RepositoryException,
        EJBException,
        PersistenceException,
        PostNotFoundException{

            if(id<1) throw new InvalidIdException();
            Category category=blogService.getCategoryOfPost(id);

            return Response.status(200).entity(category).build();
    }

    @GET
    @Path("{id}/user")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUserOfPost(@PathParam("id") int id) throws
    InvalidIdException,
    RepositoryException,
    EJBException,
    PersistenceException,
    PostNotFoundException{

        if(id<1) throw new InvalidIdException();

        try {
            Post post = blogService.getPostById(id);

            if(post==null) throw new PostNotFoundException();

            User user=post.getUser();
            return Response.status(200).entity(user).build();

        }catch (EJBException e){
            throw new EJBException(e);
        }catch (PersistenceException e){
            throw new PersistenceException(e);
        }catch (RepositoryException e) {
            throw new RepositoryException(e);
        }
    }
}

//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public void getAllPosts(@Suspended final AsyncResponse asyncResponse) throws Exception{
//        asyncResponse.setTimeout(2, TimeUnit.SECONDS);
//
//        Runnable longRunningQuery=new Runnable(){
//            public void run(){
//                List<Post> posts=null;
//
//                try {
//                    posts = blogService.getAllPosts();
//
//                }catch (Exception e){
//                    asyncResponse.resume(Response.status(500).build());
//                }
//
//                GenericEntity<List<Post>> entity=new GenericEntity<List<Post>>(posts){};
//                if(posts==null) asyncResponse.resume(Response.status(404).build());
//                else if(posts!=null) asyncResponse.resume(Response.ok().entity(entity).build());
//            }
//        };
//
//        executorService.execute(longRunningQuery);
//    }