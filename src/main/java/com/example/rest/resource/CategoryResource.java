package com.example.rest.resource;


import com.example.entities.Category;
import com.example.exception.CategoryNotFoundException;
import com.example.exception.InvalidIdException;
import com.example.exception.RepositoryException;
import com.example.service.BlogServiceRemote;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;

@Path("/category")
public class CategoryResource {

    @EJB
    BlogServiceRemote blogService;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCategoryById(@PathParam("id")int id)
    throws
            InvalidIdException,
            EJBException,
            PersistenceException,
            RepositoryException,
            CategoryNotFoundException
    {

        if(id<1) throw new InvalidIdException();

        Category category = blogService.getCategoryById(id);
        if (category == null) throw new CategoryNotFoundException();

        return Response.status(200).entity(category).build();
    }

//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response getCategoryByName(@QueryParam("name") String name)
//    throws  EJBException,
//            PersistenceException,
//            CategoryNotFoundException,
//            RepositoryException {
//
//        Category category = blogService.getCategoryByName(name);
//        if (category == null) throw new CategoryNotFoundException();
//
//        return Response.status(200).entity(category).build();
//    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllCategories(@QueryParam("name")String name)
    throws  EJBException,
            PersistenceException,
            RepositoryException,
            CategoryNotFoundException {

        if (name == null) return Response.status(200).entity(blogService.getAllCategories()).build();

        //IF CATEGORY NAME NOT NULL GET CATEGORY BY NAME
        Category category = blogService.getCategoryByName(name);
        //IF CATEGORY NOT FOUND THROW EXCEPTION
        if(category==null) throw new CategoryNotFoundException();

        return  Response.status(200).entity(category).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCategory(Category category)
    throws  EJBException,
            PersistenceException,
            RepositoryException
    {
        ValidatorFactory factory= Validation.buildDefaultValidatorFactory();
        Validator validator=factory.getValidator();

        Set<ConstraintViolation<Category>> violations=validator.validate(category);
        String validateMessage="";

        for(ConstraintViolation<Category> violation:violations){
            validateMessage+=violation.getMessage();
        }

        if(!validateMessage.equals("")){
            return Response.status(500).entity(validateMessage).build();
        }

       blogService.addCategory(category);

       return Response.status(200).entity("Category added").build();
    }


    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCategory(@PathParam("id")int id,Category category) throws EJBException,
            PersistenceException,
            InvalidIdException,
            CategoryNotFoundException,
            RepositoryException
    {
        if(id<1) throw new InvalidIdException();

        try {

            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            Validator validator = factory.getValidator();

            Set<ConstraintViolation<Category>> violations = validator.validate(category);
            String validateMessage = "";

            for (ConstraintViolation<Category> violation : violations) {
                validateMessage += violation.getMessage();
            }

            if (!validateMessage.equals("")) {
                return Response.status(500).entity(validateMessage).build();
            }

            blogService.updateCategory(id, category);
            return Response.status(204).entity("Category updated").build();

        }catch (CategoryNotFoundException e){
            throw new CategoryNotFoundException();
        }catch (PersistenceException e){
            throw new PersistenceException(e);
        }catch (EJBException e) {
            throw new EJBException(e);
        }catch(RepositoryException e){
            throw new RepositoryException(e);
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteCategory(@PathParam("id")int id)
    throws RepositoryException,CategoryNotFoundException{

        blogService.deleteCategory(id);

        return Response.status(204).entity("Category deleted").build();
    }
}
