package com.example.rest.validator;

import com.example.rest.bean.UserBean;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class ValidUserBeanValidator implements ConstraintValidator<ValidUserBean,UserBean> {

    @Override
    public void initialize(ValidUserBean constraintAnnotation){
    }

    @Override
    public boolean isValid(UserBean userBean, ConstraintValidatorContext context){

        if(userBean.getPassword().length()<3 ||
                userBean.getPassword().length()>5) return false;

        return true;
    }

}
