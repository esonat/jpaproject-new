package com.example.rest.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = {ValidUserBeanValidator.class})
@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ValidUserBean {
    String message() default "Invalid UserBean";
    Class<?>[] groups() default{};
    Class<? extends Payload>[] payload() default{};
}
