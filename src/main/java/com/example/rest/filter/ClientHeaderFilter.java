//package com.example.rest.filter;
//
//import javax.ws.rs.client.ClientRequestContext;
//import javax.ws.rs.client.ClientRequestFilter;
//import javax.ws.rs.core.Response;
//import javax.ws.rs.ext.Provider;
//import java.io.IOException;
//
///**
// * Created by sonat on 03.09.2016.
// */
//
//
//@Provider
//public class ClientHeaderFilter implements ClientRequestFilter {
//
//    @Override
//    public void filter(ClientRequestContext requestContext) throws IOException {
//
//        if(requestContext.getHeaders().get("JPAProject")==null){
//            requestContext
//                .abortWith(Response.status(Response.Status.BAD_REQUEST)
//                .entity("JPAProject header is required")
//                .build());
//        }
//    }
//}
