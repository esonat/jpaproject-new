//package com.example.rest.filter;
//
//import com.example.rest.config.BlogResourceConfig;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.ws.rs.container.ContainerRequestContext;
//import javax.ws.rs.container.ContainerRequestFilter;
//import java.io.IOException;
//
///**
// * Created by sonat on 03.09.2016.
// */
//
//@RequestLogger
//public class RequestLoggerFilter implements ContainerRequestFilter{
//Logger logger= LoggerFactory.getLogger(BlogResourceConfig.class);
//
//    @Override
//    public void filter(ContainerRequestContext requestContext) throws IOException {
//        logger.info(requestContext.getUriInfo().getPath().toString());
//        logger.info(requestContext.getMethod().toString());
//    }
//}
