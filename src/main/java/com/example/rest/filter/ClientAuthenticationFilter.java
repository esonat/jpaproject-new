//package com.example.rest.filter;
//
//import javax.ws.rs.client.ClientRequestContext;
//import javax.ws.rs.client.ClientRequestFilter;
//import javax.ws.rs.core.MultivaluedMap;
//import javax.ws.rs.ext.Provider;
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.util.Base64;
//
//
//@Provider
//public class ClientAuthenticationFilter implements ClientRequestFilter{
//
//    private final String user;
//    private final String password;
//
//    public ClientAuthenticationFilter(String user,String password){
//        this.user=user;
//        this.password=password;
//    }
//
//    public void filter(ClientRequestContext requestContext) throws IOException{
//        MultivaluedMap<String,Object> headers=requestContext.getHeaders();
//        final String basicAuthentication=getBasicAuthentication();
//        headers.add("Authorization",basicAuthentication);
//    }
//
//    private String getBasicAuthentication(){
//        String token=this.user+":"+this.password;
//        try{
//            byte[] encoded= Base64.getEncoder().encode(token.getBytes("UTF-8"));
//            return "BASIC "+new String(encoded);
//        }catch (UnsupportedEncodingException ex){
//            throw new IllegalArgumentException("Cannot encode with UTF-8",ex);
//        }
//    }
//
//
//}
//
