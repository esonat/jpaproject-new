package com.example.rest.config;

import org.glassfish.jersey.server.ResourceConfig;

import javax.annotation.Priority;
import javax.ws.rs.ext.Provider;

/**
 * Created by sonat on 06.09.2016.
 */

@Priority(2)
@Provider
public class DefaultJaxrsConfig extends ResourceConfig {

}
