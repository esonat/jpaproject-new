package com.example.rest.config;


import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.linking.DeclarativeLinkingFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.ext.Provider;
import java.util.logging.Logger;


@Provider
@ApplicationPath("/services")
public class BlogResourceConfig extends ResourceConfig {
    Logger logger=Logger.getLogger(BlogResourceConfig.class.getName());

    public BlogResourceConfig(){
        super();
        packages("com.example.rest")
        .packages("com.example.rest.exception")
        .packages("com.wordnik.swagger.jaxrs.json")
        .register(JacksonFeature.class)
        .register(new LoggingFeature(logger,LoggingFeature.Verbosity.PAYLOAD_ANY))
        .register(DeclarativeLinkingFeature.class)
        .register(MultiPartFeature.class)
        .property("disableWadl",true);

//packages ;com.example.rest.provider;com.example.rest.exception

        //.property(ServerProperties.SUBRESOURCE_LOCATOR_CACHE_SIZE,1000)
        ///.property(ServerProperties.SUBRESOURCE_LOCATOR_CACHE_AGE,60*10)
        //.register(SelectableEntityFilteringFeature.class);
        //.register(BlogMapperProvider.class)

    }
}
