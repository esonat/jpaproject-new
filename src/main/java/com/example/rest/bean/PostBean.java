package com.example.rest.bean;

import javax.validation.constraints.Size;

/**
 * Created by sonat on 01.09.2016.
 */
public class PostBean {
    @Size(min=3,max=10,message = "Post text must be between 3 and 10 characters")
    private String text;

    private String username;

    private String category;

    public PostBean(){}

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
