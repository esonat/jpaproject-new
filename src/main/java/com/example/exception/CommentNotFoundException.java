package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class CommentNotFoundException extends Exception {
    Logger logger= LoggerFactory.getLogger(CommentNotFoundException.class);

    public CommentNotFoundException(){}

    public CommentNotFoundException(String message) {
        super(message);
        logger.warn("COMMENT NOT FOUND");
    }

    public CommentNotFoundException(Throwable cause) {
        super(cause);
    }

    public CommentNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
