package com.example.exception;


public class RepositoryException extends Exception {

    private static final long serialVersionUID = 201045446026900559L;

    public RepositoryException(){}

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException(Throwable cause) {
        super(cause);
    }

    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
