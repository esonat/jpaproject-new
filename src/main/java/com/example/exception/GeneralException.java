package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sonat on 16.09.2016.
 */
public class GeneralException extends Exception {
    Logger logger= LoggerFactory.getLogger(DuplicateCategoryException.class);

    public GeneralException(String message) {
        super(message);
        logger.warn("GENERAL EXCEPTION");
    }

    public GeneralException(Throwable cause) {
        super(cause);
    }

    public GeneralException(String message, Throwable cause) {
        super(message, cause);
    }
}
