package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sonat on 17.08.2016.
 */
public class DuplicateCategoryException extends Exception {
    Logger logger= LoggerFactory.getLogger(DuplicateCategoryException.class);

    public DuplicateCategoryException(String message) {
        super(message);
        logger.warn("DUPLICATE CATEGORY");
    }

    public DuplicateCategoryException(Throwable cause) {
        super(cause);
    }

    public DuplicateCategoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
