package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sonat on 18.09.2016.
 */
public class CountryNotFoundException extends Exception {
    Logger logger= LoggerFactory.getLogger(CountryNotFoundException.class);

    public CountryNotFoundException(){}

    public CountryNotFoundException(String message) {
        super(message);
        logger.warn("COUNTRY NOT FOUND");
    }

    public CountryNotFoundException(Throwable cause) {
        super(cause);
    }

    public CountryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
