package com.example.exception;

/**
 * Created by sonat on 14.08.2016.
 */

public class UserNotFoundException extends Exception{

    private static final long serialVersionUID = 6560292459158013372L;

    public UserNotFoundException(){}

    public UserNotFoundException(String message) {
        super(message);
    }

    public UserNotFoundException(Throwable cause) {
        super(cause);
    }

    public UserNotFoundException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

}
