package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sonat on 17.08.2016.
 */
public class CategoryNotFoundException extends Exception{
    private static final long serialVersionUID = -2188597578511923999L;
    Logger logger= LoggerFactory.getLogger(ClientNotFoundException.class);

    public CategoryNotFoundException(){}

    public CategoryNotFoundException(String message) {
        super(message);
        logger.warn("CATEGORY NOT FOUND");
    }

    public CategoryNotFoundException(Throwable cause) {
        super(cause);
    }

    public CategoryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
