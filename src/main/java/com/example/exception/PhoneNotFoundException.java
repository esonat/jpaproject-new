package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sonat on 15.09.2016.
 */
public class PhoneNotFoundException extends Exception{
    Logger logger= LoggerFactory.getLogger(PhoneNotFoundException.class);

    public PhoneNotFoundException(String message) {
        super(message);
        logger.warn("Phone not found");
    }

    public PhoneNotFoundException(Throwable cause) {
        super(cause);
    }

    public PhoneNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
