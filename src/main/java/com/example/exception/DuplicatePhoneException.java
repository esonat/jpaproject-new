package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sonat on 15.09.2016.
 */
public class DuplicatePhoneException extends Exception {
    Logger logger= LoggerFactory.getLogger(DuplicatePhoneException.class);

    public DuplicatePhoneException(String message) {
        super(message);
        logger.warn("DUPLICATE PHONE");
    }

    public DuplicatePhoneException(Throwable cause) {
        super(cause);
    }

    public DuplicatePhoneException(String message, Throwable cause) {
        super(message, cause);
    }
}
