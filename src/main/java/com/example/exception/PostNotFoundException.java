package com.example.exception;

/**
 * Created by sonat on 02.09.2016.
 */
public class PostNotFoundException extends Exception {

    public PostNotFoundException(){}

    public PostNotFoundException(String message) {
        super(message);
    }

    public PostNotFoundException(Throwable cause) {
        super(cause);
    }

    public PostNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
