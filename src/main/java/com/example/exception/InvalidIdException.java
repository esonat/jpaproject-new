package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InvalidIdException extends Exception{
    Logger logger= LoggerFactory.getLogger(InvalidIdException.class);

    public InvalidIdException(){}

    public InvalidIdException(String message) {
        super(message);
        logger.info("Invalid Id");
    }

    public InvalidIdException(Throwable cause) {
        super(cause);
    }

    public InvalidIdException(String message, Throwable cause) {
        super(message, cause);
    }
}
