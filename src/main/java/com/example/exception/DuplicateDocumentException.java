package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DuplicateDocumentException extends Exception {
    Logger logger= LoggerFactory.getLogger(DuplicateCategoryException.class);

    public DuplicateDocumentException(){}

    public DuplicateDocumentException(String message) {
        super(message);
        logger.warn("DUPLICATE CATEGORY");
    }

    public DuplicateDocumentException(Throwable cause) {
        super(cause);
    }

    public DuplicateDocumentException(String message, Throwable cause) {
        super(message, cause);
    }
}
