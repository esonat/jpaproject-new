package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DuplicateUsernameException extends Exception {
    Logger logger= LoggerFactory.getLogger(DuplicateCategoryException.class);

    public DuplicateUsernameException(String message) {
        super(message);
        logger.warn("DUPLICATE USERNAME");
    }

    public DuplicateUsernameException(Throwable cause) {
        super(cause);
    }

    public DuplicateUsernameException(String message, Throwable cause) {
        super(message, cause);
    }
}
