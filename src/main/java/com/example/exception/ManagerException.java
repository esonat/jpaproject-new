package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class ManagerException extends Exception{
    Logger logger= LoggerFactory.getLogger(ManagerException.class);

    public ManagerException(String message) {
        super(message);
        logger.warn("GENERAL EXCEPTION");
    }

    public ManagerException(Throwable cause) {
        super(cause);
    }

    public ManagerException(String message, Throwable cause) {
        super(message, cause);
    }
}
