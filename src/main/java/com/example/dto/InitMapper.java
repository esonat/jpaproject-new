package com.example.dto;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonat on 13.08.2016.
 */
public class InitMapper {

    public static Mapper getMapper(){

        List<String> list=new ArrayList<String>();
        list.add("dozerMapping.xml");

        Mapper mapper = new DozerBeanMapper(list);
        return mapper;
    }
}
