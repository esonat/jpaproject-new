package com.example.dto;

import com.example.entities.Category;
import org.dozer.DozerConverter;

/**
 * Created by sonat on 14.08.2016.
 */
    public class CategoryToNameConverter  extends DozerConverter<Category,String> {

        public CategoryToNameConverter() {
            super(Category.class, String.class);
        }

        public String convertTo(Category source, String destination){
            String name=null;
            try {
                name=source.getName();
            }catch (Exception e){
                e.printStackTrace();
            }
            return name;
        }

        public Category convertFrom(String source, Category destination) {
            Category category=new Category();
            category.setName(source);

            return category;
        }

    }
