//package com.example.dto;
//
//import com.example.entities.Category;
//import org.dozer.DozerConverter;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by sonat on 21.08.2016.
// */
//public class CategoryListToNameListConverter extends DozerConverter<List<Category>,List<String>> {
//    public CategoryListToNameListConverter() {
//        super();
//    }
//
//    public List<String> convertTo(List<Category> source, List<String> destination){
//        List<String> list=null;
//
//        try {
//            for(Category category:source){
//                list.add(category.getName());
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return list;
//    }
//
//    public List<Category> convertFrom(List<String> source, List<Category> destination) {
//        List<Category> list=null;
//        for(String item:source){
//            Category category=new Category();
//            category.setName(item);
//            list.add(category);
//        }
//        return list;
//    }
//
//}
