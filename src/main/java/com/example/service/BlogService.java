package com.example.service;

import com.example.entities.*;
import com.example.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


//@EJB(beanName = "BlogService", name = "BlogService")
//EJB(beanInterface = BlogServiceRemote.class,name="BlogService",beanName = "BlogService")
@Stateless
public class BlogService implements BlogServiceRemote {
    public static final long REFRESH_THRESHOLD = 300000;
    public Logger logger = LoggerFactory.getLogger(BlogService.class);

    @PersistenceContext(unitName = "BlogPersistence")
    private EntityManager em;

    @Override
    public void addCategory(String name) throws RepositoryException {
        String error="Exception while persisting category.Name:"+name;

        try {
            Category category = new Category();
            category.setName(name);

            em.persist(category);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public void addCategory(Category category) throws RepositoryException {
        String error="Exception while persisting category";

        try {
           em.persist(category);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public void addChildComment(int parentId, Comment comment) throws CommentNotFoundException,RepositoryException {
            String error="Exception while finding parent comment by id.Id:"+parentId;

            Comment parent=null;

            try {
                parent = em.find(Comment.class, parentId);

            }catch(EJBException e){
                logger.error(error+e.getMessage());
            }catch (EntityNotFoundException | NoResultException e) {
                logger.error(error+e.getMessage());
            }catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            }catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }

            if (parent == null) throw new CommentNotFoundException("Parent not found");

            if (parent.getChildren() == null) parent.setChildren(new ArrayList<Comment>());

        try{
            error="Exception while persisting parent comment.Id:"+parent.getId();

            parent.getChildren().add(comment);
            comment.setParent(parent);

            em.persist(parent);

        } catch (EJBException e) {
            logger.error(error+e.getMessage());
        } catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        } catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public void addCity(int countryId, City city) throws CountryNotFoundException,RepositoryException{
        String error="Exception while finding country by id.Id:"+countryId;

            Country country=null;

            try {
                country = em.find(Country.class, countryId);

            }catch(EJBException e){
                logger.error(error+e.getMessage());
            }catch (EntityNotFoundException | NoResultException e) {
                logger.error(error+e.getMessage());
            }catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            }catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }

            if (country == null) throw new CountryNotFoundException("Country with id:"+countryId+" not found");

            if (country.getCities() == null) {
                country.setCities(new ArrayList<City>());
            }

            error="Exception while persisting city.Name:"+city.getName();

            try{
                country.getCities().add(city);
                city.setCountry(country);

                em.persist(city);

            }catch(EJBException e){
                logger.error(error+e.getMessage());
            }catch (EntityNotFoundException | NoResultException e) {
                logger.error(error+e.getMessage());
            }catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            }catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }
    }

    @Override
    public String addClient(Client client, User user) throws RepositoryException {
        String error="Exception while persisting client";
        String id=null;

        try {
                client.setUser(user);
                em.persist(client);

                id=client.getId();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        return id;
    }

    @Override
    public void addComment(Comment comment) throws RepositoryException {
        String error="Exception persisting comment";

        try {
            em.persist(comment);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

    }

    @Override
    public void addCountry(Country country) throws RepositoryException {
        String error="Exception while persisting country.Name:"+country.getName();

        try {
            em.persist(country);

        } catch (EJBException e) {
            logger.error(error+e.getMessage());
        } catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        } catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public Document addDocument(Document document) throws RepositoryException,DuplicateDocumentException {
        String error="Exception while persisting document";

        try {

            document.setStatus(DocumentStatus.APPROVED);
            em.persist(document);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        return document;
    }

    @Override
    public int addPost(Post post, String categoryName, String username, Document document)
    throws  CategoryNotFoundException,
            UserNotFoundException,
            RepositoryException
    {
        String error="Exception while getting category by name.Name:"+categoryName;

            if(categoryName==null)  throw new RepositoryException("CategoryName cannot be null");
            if(username==null)      throw new RepositoryException("Username cannot be null");

            Category category=null;

            try {
                category = getCategoryByName(categoryName);
            }catch(EJBException e){
                logger.error(error+e.getMessage());
            }catch (EntityNotFoundException | NoResultException e) {
                logger.error(error+e.getMessage());
            }catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            }catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }

            if(category==null) throw new CategoryNotFoundException("Category with name:"+categoryName+" not found");

            User user=null;

            error="Exception getting by username.Username:"+username;

            try {
                user = getUserByUsername(username);

            }catch(EJBException e){
                logger.error(error+e.getMessage());
            }catch (EntityNotFoundException | NoResultException e) {
                logger.error(error+e.getMessage());
            }catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            }catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }

            if (user == null)  throw new UserNotFoundException();
            if (user.getPosts() == null) user.setPosts(new ArrayList<Post>());

            user.getPosts().add(post);
            post.setUser(user);

            if (category.getPosts() == null) category.setPosts(new ArrayList<Post>());

            category.getPosts().add(post);
            post.setCategory(category);

            //document.getPosts().add(post);
            post.setDatetime(new Date());
            if (document != null) post.setDocument(document);


          error="Exception persisting post";
            int postId=0;

        try{

            em.persist(post);
            em.flush();

            postId=post.getId();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        return postId;
    }

    @Override
    public void addPostComment(int postId, Comment comment) throws PostNotFoundException,RepositoryException {

            String error="Exception while finding post.Id:"+postId;

            Post post=null;

            try {
                post = em.find(Post.class, postId);

            } catch (EJBException e) {
                logger.error(error+e.getMessage());
            } catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            } catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }

            if (post == null) throw new PostNotFoundException();
            if (post.getComments() == null) post.setComments(new ArrayList<Comment>());

            error="Exception while persisting post.Id:"+postId;

            try {
                post.getComments().add(comment);
                comment.setPost(post);

                em.persist(post);
            } catch (EJBException e) {
                logger.error(error+e.getMessage());
            } catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            } catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }
    }

    @Override
    public void addRole(Role role) throws RepositoryException {
        String error="Exception while persisting role.Role:"+role.getName();

        try {
            em.persist(role);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        } catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public void addTask(Task task) throws RepositoryException {
        String error="Exception while persisting task.Type:"+task.getType();

        try {
            em.persist(task);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityExistsException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e){
            logger.error(error+e.getMessage());
        } catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public void addTaskAndClients(Task task, List<Client> clients) throws RepositoryException{
        String error="Exception while persisting task";

        if(task.getCategory().equals(TaskCategory.MESSAGETASK)){

                if(clients!=null){
                    for(Client client:clients){
                        task.getClients().add(client);
                        client.getTasks().add(task);
                    }
                    try {
                        em.persist(task);

                    }catch(EJBException e){
                        logger.error(error+e.getMessage());
                    }catch (EntityNotFoundException | NoResultException e) {
                        logger.error(error+e.getMessage());
                    }catch (PersistenceException e) {
                        logger.error(error+e.getMessage());
                    }catch (Exception e) {
                        throw new RepositoryException(error+e.getMessage());
                    }

                }
            }else {

                try {
                    em.persist(task);

                }catch(EJBException e){
                    logger.error(error+e.getMessage());
                }catch (EntityNotFoundException | NoResultException e) {
                    logger.error(error+e.getMessage());
                }catch (PersistenceException e) {
                    logger.error(error+e.getMessage());
                }catch (Exception e) {
                    throw new RepositoryException(error+e.getMessage());
                }

        }
    }

    @Override
    public void addUser(User user, List<String> roleList, List<Phone> phones) throws RepositoryException
                                                                                ,DuplicatePhoneException
                                                                                ,DuplicateUsernameException{
        String error;

        String username=user.getUserLoginInfo().getUsername();
        if(getUserByUsername(username)!=null) throw new DuplicateUsernameException("Duplicate username:"+username);

       // user.getUserinfo().getPhones().addAll(phones);

        for (Phone phone : phones) {

            error="Exception while persisting phone.Number:"+phone.getNum();

            if (findPhoneByNumber(phone.getNum()) != null) {
                throw new DuplicatePhoneException("Duplicate Phone. Number:"+phone.getNum());
            }

            try {
                phone.setUser(user);
                em.persist(phone);

            }catch (EJBException e){
                logger.error(error+e.getMessage());
            }catch (PersistenceException e){
                logger.error(error+e.getMessage());
            }catch (Exception e){
                throw new RepositoryException(error+e.getMessage());
            }
        }

        error="Exception while persisting user";
        try{
            em.persist(user);

        }catch(EJBException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }


        List<UserRole> userRoleList=new ArrayList<UserRole>();

        for (String roleName : roleList) {

            error="Exception while finding role by name:"+roleName;

            boolean exists = false;
            Role role=null;
            try {
                role=findRoleByName(roleName);
                if(role!=null) exists=true;

            } catch (EJBException e) {
                logger.error(error+e.getMessage());
            } catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            } catch (Exception e) {
               throw new RepositoryException(error+e.getMessage());
            }


            if(exists){
                    UserRole userRole=new UserRole();
                    userRole.setRole(role);
                    userRole.setUser(user);
                    role.getUserRoles().add(userRole);
                    user.getUserRoles().add(userRole);

                    error="Exception while updating role.Name:"+role.getName();

                    try{

                        em.merge(role);

                    } catch (EJBException e) {
                        logger.error(error+e.getMessage());
                    } catch (PersistenceException e) {
                        logger.error(error+e.getMessage());
                    } catch (Exception e) {
                        throw new RepositoryException(error+e.getMessage());
                    }

            }else if (!exists) {

                try {
                    error="Exception while persisting role.Name:"+roleName;

                    role=new Role(roleName);

                    UserRole userRole=new UserRole();
                    userRole.setRole(role);
                    userRole.setUser(user);
                    role.getUserRoles().add(userRole);

                    em.persist(role);

                } catch (EJBException e) {
                    logger.error(error+e.getMessage());
                } catch (PersistenceException e) {
                    logger.error(error+e.getMessage());
                } catch (Exception e) {
                    throw new RepositoryException(error+e.getMessage());
                }
            }
        }



    }

        @Override
    public void deleteCategory(int id) throws RepositoryException,CategoryNotFoundException{
        String error="Exception while deleting category.Id:"+id;
        Category category=null;

        try{
            category=em.find(Category.class,id);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

            if(category==null) throw new CategoryNotFoundException("Cannot find the category to be deleted");

        error="Exception while removing category";

        try{
            em.remove(category);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        }

    @Override
    public void deleteComment(int commentId) throws RepositoryException,CommentNotFoundException {

        String error="Exception while getting comment by id.Id:"+commentId;
        Comment comment=null;

        try {
            comment = em.find(Comment.class, commentId);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }


        if (comment == null) throw new CommentNotFoundException("Comment to be deleted not found");

        error="Exception removin comment.ID:"+commentId;

        try {
            em.remove(comment);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

    }

    @Override
    public Category findCategoryByName(String name) throws RepositoryException{
        String error="Exception while finding category by name.Name:"+name;
        List<Category> list=null;

        try {
            TypedQuery<Category> query = em.createQuery("SELECT c FROM Category c WHERE c.name = ?1", Category.class);
            query.setParameter(1, name);
            query.setMaxResults(1);

            list = query.getResultList();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException e) {
            logger.error(error+e.getMessage());
        }catch (NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        if (list == null || list.size() == 0) return null;
        return list.get(0);
    }

    @Override
    public Phone findPhoneByNumber(String number) throws RepositoryException{
        String error="Exception finding phone by number.Number:"+number;
        Phone phone=null;

        try {
            phone=em.find(Phone.class,number);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        return phone;
    }

    @Override
    public Role findRoleByName(String name) throws RepositoryException{
        String error="Exception while finding role by name.Name:"+name;
        Role role=null;

        try {
            List<Role> roles=(List<Role>)em.createQuery("SELECT r FROM Role r WHERE r.name= :name")
                    .setParameter("name",name)
                    .getResultList();

            if(roles==null || roles.size()==0) return null;

            role=roles.get(0);

        }catch(EJBException | EntityNotFoundException | NoResultException e) {
            throw new NoResultException(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        return role;
    }

    @Override
    public User findUserById(int id) throws RepositoryException{
        String error="Exception while finding user by id.Id:"+id;
        User user=null;

        try{
            user=em.find(User.class,id);
            return user;

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        return user;
    }

    @Override
    public List<Category> getAllCategories() throws RepositoryException {
        String error="Exception while getting all categories";
        List<Category> categories=null;

        try {
            categories = (List<Category>) em.createQuery("SELECT c FROM Category c")
                    .getResultList();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException e) {
            logger.error(error+e.getMessage());
        }catch (NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        if (categories == null || categories.size() == 0) return null;
        return categories;
    }

    @Override
    public List<Document> getAllDocuments() throws RepositoryException{
        String error="Exception while getting all documents";
        List<Document> documents=null;

        try {
            documents = (List<Document>) em.createQuery("SELECT d FROM Document d").getResultList();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        if (documents == null || documents.size() == 0) return null;
        return documents;
    }

    @Override
    public List<Phone> getAllPhones() throws RepositoryException{
        String error="Exception while getting all phones";
        List<Phone> phones=null;

        try {
            phones = em.createQuery("SELECT p FROM Phone p")
                    .getResultList();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        if (phones == null || phones.size() == 0) return null;
        return phones;
    }

    @Override
    public List<Post> getAllPosts() throws RepositoryException{
        String error="Exception while getting all posts";
        List<Post> posts=null;

        try {
            posts = (List<Post>) em.createQuery("SELECT p FROM Post p").getResultList();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        if (posts == null || posts.size() == 0) return null;
        return posts;
    }

    @Override
    public List<User> getAllUsers() throws RepositoryException {
        String error="Exception while getting all users";
        List<User> users=null;

        try {
            users = (List<User>) em.createQuery("SELECT u FROM User u").getResultList();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        if (users == null || users.size() == 0) return null;
        return users;
    }

    @Override
    public Category getCategoryById(int id) throws RepositoryException {
        String error="Exception while getting category by id.Id:"+id;
        Category category=null;

        try {
            category = em.find(Category.class, id);

        } catch (EJBException e) {
            logger.error(error + e.getMessage());
        } catch (EntityNotFoundException | NoResultException e) {
            logger.error(error + e.getMessage());
        } catch (PersistenceException e) {
            logger.error(error + e.getMessage());
        } catch (Exception e) {
            throw new RepositoryException(error + e.getMessage());
        }
        return category;
    }

    @Override
    public Category getCategoryByName(String name) throws RepositoryException{
        String error="Exception while getting category by name.Name:"+name;
        List<Category> categories=null;

        try{
            categories=em.createQuery("SELECT c FROM Category c WHERE c.name= :name")
                    .setParameter("name",name)
                    .getResultList();


        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        if(categories==null || categories.size()==0) return null;

        return categories.get(0);
    }

    @Override
    public Category getCategoryOfPost(int id) throws RepositoryException,PostNotFoundException{
        String error="Exception while finding post.Id:"+id;
        Post post=null;

        try {
            post = em.find(Post.class, id);
        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        if (post == null) throw new PostNotFoundException();
        return post.getCategory();
    }

    @Override
    public City getCityByName(String name) throws  RepositoryException {
        String error="Exception while getting city by name.Name:"+name;
        List<City> cities=null;

        try {
            cities = (List<City>) em.createQuery("SELECT c FROM City c WHERE c.name = :name")
                    .setParameter("name", name)
                    .getResultList();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        if (cities == null || cities.size() == 0) return null;
        return cities.get(0);
    }

    @Override
    public Client getClientById(String clientId) throws RepositoryException {
        String error="Exception while getting user by client id.ClientId:"+clientId;

        Client client=null;

        try {

            client=em.find(Client.class, clientId);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException e) {
            logger.error(error+e.getMessage());
        }catch (NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        } catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        return client;
    }

    @Override
    public Country getCountryByName(String name) throws RepositoryException {
        String error="Exception while getting country by name.Name:"+name;

        Country country=null;

        try {
            TypedQuery<Country> query = em.createNamedQuery("Country.getByName", Country.class)
                            .setParameter("name", name);

            List<Country> results = query.getResultList();
            if (results == null) return null;

            country=results.get(0);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        return country;
    }

    @Override
    public Post getPostById(int id) throws RepositoryException{
        String error="Exception while getting post by id.Id:"+id;

        Post post=null;

        try {
            post=em.find(Post.class,id);


        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        if(post==null) return null;

        post.getCategory();
        post.getUser();

        return post;
    }

    @Override
    public List<Post> getPostsByUser(String name) throws RepositoryException {
        String error="Exception getting posts by user.Username:"+name;

        List<Post> posts=null;

        try {
            posts = em.createNamedQuery("Post.findByUserName", Post.class)
                    .setParameter("name", name)
                    .getResultList();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        if (posts == null || posts.size() == 0) return null;

        return posts;
    }

    @Override
    public Task getTaskByType(String type) throws RepositoryException {
        String error="Exception while getting task by type.Type:"+type;
        List<Task> list=null;

        try {
            list = em.createQuery("SELECT t FROM Task t WHERE t.type= :type")
                    .setParameter("type", type)
                    .getResultList();


        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException e) {
            logger.error(error+e.getMessage());
        }catch (NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            logger.error(error+e.getMessage());
            throw new RepositoryException(error+e.getMessage());
        }

        if (list == null || list.size() == 0) return null;
        return list.get(0);
    }

    @Override
    public User getUserByClientId(String clientId) throws RepositoryException {
        String error="Exception while getting user by client id.ClientId:"+clientId;
        List<User> users=null;

        try {
            users = em.createQuery("SELECT c.user FROM Client c WHERE c.id = :clientId")
                    .setParameter("clientId", clientId)
                    .getResultList();



        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException e) {
            logger.error(error+e.getMessage());
        }catch (NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            logger.error(error + e.getMessage());
            throw new RepositoryException(error + e.getMessage());
        }

        if (users == null || users.size() == 0)  return null;
        return users.get(0);
    }

    @Override
    public User getUserByName(String name) throws RepositoryException {
        String error="Exception while getting user by name.Name:"+name;
        List<User> users=null;

        try {
            users = (List<User>) em.createQuery("SELECT u FROM User u WHERE u.userinfo.name = :name")
                    .setParameter("name", name)
                    .getResultList();

            if (users == null || users.size() == 0) return null;

            return users.get(0);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
        if (users == null || users.size() == 0) return null;

        return users.get(0);
    }

    @Override
    public User getUserByUsername(String username) throws RepositoryException {
        String error="Exception while getting user by username:"+username;
        List<User> users=null;

        try {
            users = (List<User>) em.createQuery("SELECT u FROM User u WHERE u.userLoginInfo.username = :username")
                    .setParameter("username", username)
                    .getResultList();

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        if (users == null || users.size() == 0)  return null;
        return users.get(0);
    }

    @Override
    public void removeClient(String clientId) throws RepositoryException {
        String error="Exception while finding client.ClientId:"+clientId;
        Client client=null;

        try {
            client = em.find(Client.class, clientId);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        error="Exception while removing client.ClientId:"+clientId;

        try{
            em.remove(client);
        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public void removeTask(Task task) throws RepositoryException,TaskNotFoundException{
        String error="Exception while finding task.Type:"+task.getId();

        try {
            task = em.find(Task.class, task.getId());

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException e){
            logger.error(error+e.getMessage());
        }catch (NoResultException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e){
            logger.error(error+e.getMessage());
        }catch (Exception e){
            throw new RepositoryException(error+e.getMessage());
        }

        if (task == null) throw new TaskNotFoundException("Type:"+task.getType());

        error="Exception while removing task";

        try{
            em.remove(task);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException e){
            logger.error(error+e.getMessage());
        }catch (NoResultException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e){
            logger.error(error+e.getMessage());
        }catch (Exception e){
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public void updateCategory(int id, Category category) throws
            RepositoryException,
            CategoryNotFoundException{

        String error="Exception while updating category.Name:"+category.getName();
        Category updated=em.find(Category.class,id);

        if(updated==null)
            throw new CategoryNotFoundException();
        if(updated.getName().equals(category.getName()))
            throw new RepositoryException("Category names are equal");

        try{
            updated.setName(category.getName());
            em.merge(updated);

        }catch (RollbackException e){
            logger.error(error+e.getMessage());
        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public void updateClient(String clientId, int port, String username, String clientRole, boolean alive)
            throws ClientNotFoundException,RepositoryException
    {
        String error="Exception wile updating client.ClientId:"+clientId;

        Client client=null;

        try{
            client=em.find(Client.class,clientId);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

        if(client==null) throw new ClientNotFoundException("Client with id:"+clientId+" not found");

        User user=null;

        try {
            user = getUserByUsername(username);
        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e){
            logger.error(error+e.getMessage());
        }

        client.setPort(port);
        client.setUser(user);
        client.setClientRole(clientRole);
        if(alive) {
            client.setLatestMessage(new Date());
        }else {
            client.setLatestActivity(new Date());
        }

        error="Exception while updating client";

        try{
            em.merge(client);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public void updateClient(String clientId, Client client) throws RepositoryException {
        String error="Exception while updating client.ClientId:"+clientId;

        try {
            client=em.find(Client.class,clientId);
            client.setLatestActivity(new Date());

            em.merge(client);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }
    }

    @Override
    public void updatePost(int id, String text, String username, String categoryName)
            throws  RepositoryException,
                    PostNotFoundException,
                    UserNotFoundException,
                    CategoryNotFoundException,
                    EJBException,
                    PersistenceException
    {
            String error="Exception while getting post by id.Id:"+id;

            Post post=null;
            User user=null;
            Category category=null;

            try {
                post = getPostById(id);
            }catch(EJBException e){
                logger.error(error+e.getMessage());
            }catch (EntityNotFoundException | NoResultException e) {
                logger.error(error+e.getMessage());
            }catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            }catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }

            error="Exception while getting user by username.Username:"+username;


            try{
                user = getUserByUsername(username);
            }catch(EJBException e){
                logger.error(error+e.getMessage());
            }catch (EntityNotFoundException | NoResultException e) {
                logger.error(error+e.getMessage());
            }catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            }catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }

            error="Exception while getting category by name.Name:"+categoryName;

            try{
                category = getCategoryByName(categoryName);
            }catch(EJBException e){
                logger.error(error+e.getMessage());
            }catch (EntityNotFoundException | NoResultException e) {
                logger.error(error+e.getMessage());
            }catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            }catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }

            if (post == null)       throw new PostNotFoundException("Post not found");
            if (user == null)       throw new UserNotFoundException("User not found");
            if (category == null)   throw new CategoryNotFoundException("Category not found");

            error="Exception while updating post.Id:"+id;

            try{
                post.setUser(user);
                post.setCategory(category);
                post.setText(text);
                post.setDatetime(new Date());

                em.merge(post);

            }catch(EJBException e){
                logger.error(error+e.getMessage());
            }catch (EntityNotFoundException | NoResultException e) {
                logger.error(error+e.getMessage());
            }catch (PersistenceException e) {
                logger.error(error+e.getMessage());
            }catch (Exception e) {
                throw new RepositoryException(error+e.getMessage());
            }

    }

    @Override
    public void updatePost(int id, Document document)
            throws RepositoryException,PostNotFoundException{
        String error="Exception while getting post by id.Id:"+id;
        Post post=null;

        try {

           post = getPostById(id);

       }catch(EJBException e){
           logger.error(error+e.getMessage());
       }catch (EntityNotFoundException | NoResultException e) {
           logger.error(error+e.getMessage());
       }catch (PersistenceException e) {
           logger.error(error+e.getMessage());
       }catch (Exception e) {
           throw new RepositoryException(error+e.getMessage());
       }

        if (post == null) throw new PostNotFoundException();
        post.setDocument(document);

        error="Exception while updating post.Id:"+id;

        try{
            em.merge(post);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (EntityNotFoundException | NoResultException e) {
            logger.error(error+e.getMessage());
        }catch (PersistenceException e) {
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            throw new RepositoryException(error+e.getMessage());
        }

    }

    @Override
    public void updateTask(Task task) throws RepositoryException {
        String error="Exception while updating task.TaskId:"+task.getId();

        try {
                em.merge(task);

        }catch(EJBException e){
            logger.error(error+e.getMessage());
        }catch (PersistenceException e){
            logger.error(error+e.getMessage());
            logger.error(error+e.getMessage());
        }catch (Exception e) {
            logger.error(error+e.getMessage());
            throw new RepositoryException(error+e.getMessage());
        }
    }

}
