package com.example.service;

import com.example.entities.*;
import com.example.exception.*;

import javax.ejb.Remote;
import java.util.List;

/**
 * Created by sonat on 17.09.2016.
 */

@Remote
public interface BlogServiceRemote{

    void addCategory(String name) throws RepositoryException;

    void addCategory(Category category) throws RepositoryException;

    void addChildComment(int parentId, Comment comment) throws CommentNotFoundException,RepositoryException;

    void addCity(int countryId, City city) throws CountryNotFoundException,RepositoryException;

    String addClient(Client client, User user) throws RepositoryException;

    void addComment(Comment comment) throws RepositoryException;

    void addCountry(Country country) throws RepositoryException;

    Document addDocument(Document document) throws RepositoryException,DuplicateDocumentException;

    int addPost(Post post, String categoryName, String username, Document document) throws CategoryNotFoundException,
            UserNotFoundException,RepositoryException;

    void addPostComment(int postId, Comment comment) throws PostNotFoundException,RepositoryException ;

    void addRole(Role role) throws RepositoryException;

    void addTask(Task task) throws RepositoryException;

    void addTaskAndClients(Task task, List<Client> clients) throws RepositoryException;

    void addUser(User user, List<String> roleList, List<Phone> phones) throws RepositoryException,
            DuplicatePhoneException,
            DuplicateUsernameException;

    void deleteCategory(int id) throws RepositoryException,CategoryNotFoundException;

    void deleteComment(int commentId) throws RepositoryException,CommentNotFoundException;

    Category findCategoryByName(String name) throws RepositoryException;

    Phone findPhoneByNumber(String number) throws RepositoryException;

    Role findRoleByName(String name) throws RepositoryException;

    User findUserById(int id) throws RepositoryException;

    List<Category> getAllCategories() throws RepositoryException;

    List<Document> getAllDocuments() throws RepositoryException;

    List<Phone> getAllPhones() throws RepositoryException;

    List<Post> getAllPosts() throws RepositoryException;

    List<User> getAllUsers() throws RepositoryException;

    Category getCategoryById(int id) throws RepositoryException;

    Category getCategoryByName(String name) throws RepositoryException;

    Category getCategoryOfPost(int id) throws PostNotFoundException,RepositoryException;

    City getCityByName(String name) throws RepositoryException;

    Client getClientById(String clientId) throws RepositoryException;

    Country getCountryByName(String name) throws RepositoryException;

    Post getPostById(int id) throws  RepositoryException;

    List<Post> getPostsByUser(String name) throws RepositoryException;

    Task getTaskByType(String type) throws RepositoryException;

    User getUserByClientId(String clientId) throws RepositoryException;

    User getUserByName(String name) throws RepositoryException;

    User getUserByUsername(String username) throws RepositoryException;

    void removeClient(String clientId) throws RepositoryException;

    void removeTask(Task task) throws RepositoryException,TaskNotFoundException;

    void updateCategory(int id, Category category) throws RepositoryException,CategoryNotFoundException;

    void updateClient(String clientId, int port, String username, String clientRole, boolean alive)
            throws ClientNotFoundException,RepositoryException;

    void updateClient(String clientId, Client client) throws RepositoryException;

    void updatePost(int id, String text, String username, String categoryName) throws
            RepositoryException,PostNotFoundException, UserNotFoundException, CategoryNotFoundException;

    void updatePost(int id, Document document)  throws RepositoryException,PostNotFoundException;

    void updateTask(Task task) throws RepositoryException;
}
