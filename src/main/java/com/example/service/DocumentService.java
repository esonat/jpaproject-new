package com.example.service;

import com.example.entities.Document;
import com.example.entities.DocumentStatus;
import com.example.entities.DocumentType;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

/**
 * Created by sonat on 12.08.2016.
 */
@Stateful
@TransactionAttribute(TransactionAttributeType.NEVER)
public class DocumentService {

    @PersistenceContext(unitName = "BlogPersistence",
        type= PersistenceContextType.EXTENDED)
    EntityManager em;

    public Document setDocumentContent(byte[] content){
        Document document=new Document();
        document.setContent(content);

        return document;
        //return document;
    }

    public void setDocumentType(Document document,DocumentType documentType){
        document.setType(documentType);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Document approveDocument(Document document){
        document.setStatus(DocumentStatus.APPROVED);
        em.persist(document);

        return document;
    }


}
