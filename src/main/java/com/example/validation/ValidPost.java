package com.example.validation;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = {ValidPostValidator.class})
@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ValidPost {
    String message() default "Invalid Post";
    Class<?>[] groups() default{};
    Class<? extends Payload>[] payload() default{};
}
