package com.example.validation;

import com.example.entities.Category;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by sonat on 02.09.2016.
 */
public class ValidCategoryValidator implements ConstraintValidator<ValidCategory,Category>{

    @Override
    public void initialize(ValidCategory constraintAnnotation){

    }

    @Override
    public boolean isValid(Category category, ConstraintValidatorContext context){

        if(category.getName().length()<3 || category.getName().length()>15) return false;

        return true;
    }
}
