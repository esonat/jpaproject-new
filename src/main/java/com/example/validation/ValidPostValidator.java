package com.example.validation;

import com.example.entities.Post;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by sonat on 02.09.2016.
 */
public class ValidPostValidator implements ConstraintValidator<ValidPost,Post> {

    @Override
    public void initialize(ValidPost constraintAnnotation){

    }

    @Override
    public boolean isValid(Post post, ConstraintValidatorContext context){

        if(post.getText()==null || post.getText().length()==0)      return false;
        if(post.getText().length()<3) return false;

        return true;
    }
}
