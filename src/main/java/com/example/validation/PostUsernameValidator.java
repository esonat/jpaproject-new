package com.example.validation;

import com.example.entities.User;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by sonat on 10.08.2016.
 */
public class PostUsernameValidator implements ConstraintValidator<PostUsername,User>{

    public void initialize(PostUsername constraint){
    }

    public boolean isValid(User value, ConstraintValidatorContext ctx){
        if(value==null)
            return true;

        if(value.getUserLoginInfo().getUsername().equals("deneme"))
            return false;

        return true;
    }
}

