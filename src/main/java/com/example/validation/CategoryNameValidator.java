package com.example.validation;

import com.example.entities.Category;
import com.example.service.BlogServiceRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class CategoryNameValidator implements ConstraintValidator<CategoryName,String> {

    Logger logger= LoggerFactory.getLogger(CategoryNameValidator.class);
    BlogServiceRemote blogService;

    public CategoryNameValidator(){
        try {
             InitialContext context = new InitialContext();
             blogService = (BlogServiceRemote) context.lookup("java:global/Blog/BlogService!com.example.service.BlogServiceRemote");
                     //"java:global/Blog/BlogService!com.example.service.BlogServiceRemote");

        }catch (NamingException e){
            logger.error("Naming Exception:"+e.getCause()+e.getMessage());
        }
    }

    public void initialize(CategoryName constraint){
    }

    public boolean isValid(String value, ConstraintValidatorContext ctx){
        if(value==null) return false;

        try {
                for (Category item : blogService.getAllCategories()) {
                    if (item.getName().equals(value)) return false;
                }

        }catch (Exception e){
            logger.error("Exception in CategoryNameValidator");
            return false;
        }

        return true;
    }
}
