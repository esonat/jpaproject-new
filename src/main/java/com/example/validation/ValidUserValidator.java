package com.example.validation;

import com.example.entities.User;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by sonat on 15.09.2016.
 */
public class ValidUserValidator implements ConstraintValidator<ValidUser,User>{
    @Override
    public void initialize(ValidUser constraintAnnotation){

    }

    @Override
    public boolean isValid(User user, ConstraintValidatorContext context){

        if(user.getUserLoginInfo().getPassword().length()<3 ||
                user.getUserLoginInfo().getPassword().length()>5){
            return false;
        }

        return true;
    }

}
