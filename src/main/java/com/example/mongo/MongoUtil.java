package com.example.mongo;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.lang.StringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MongoUtil {
    static Logger logger= LoggerFactory.getLogger(MongoUtil.class);


    public static List<Document> readMap(Map<String,Object> map) {

        List<Document> result = new ArrayList<Document>();

        //try {


            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                String[] arr = null;
                int len = 0;
                if (key.contains(".")) {
                    arr = StringUtils.split(key,'.');
                    len = arr.length;

                    Document[] docArr = new Document[len];
//                    for(int index=0;index<docArr.length;index++){
//                        docArr[index]=new Document();
//                    }

                    for (int index = len - 1; index >= 0; index--) {
                        if (index == len - 1) docArr[index] = new Document(arr[index], value);
                        else {
                            docArr[index] = new Document();
                            docArr[index].append(arr[index], docArr[index + 1]);
                        }
                    }

                    result.add(docArr[0]);
                } else {
                    result.add(new Document(key, value));
                }
            }
//        }catch (Exception e){
//            logger.warn(e.getMessage()+e.getCause());
//        }

    return result;
    }

    public static void update(String collection,Map<String,Object> map){
        MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
        MongoDatabase DB = mongoClient.getDatabase("JPAProject");


    }

    public static void write(String collection,Map<String,Object> map){
   //     try{
            MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
            MongoDatabase DB = mongoClient.getDatabase("JPAProject");

            BasicDBObjectBuilder documentBuilder=new BasicDBObjectBuilder();
//            DB.getCollection(collection).insertOne(documentBuilder.get());


            List<Document> getDocumentList=readMap(map);

            for(Document document:getDocumentList){
                DB.getCollection(collection).insertOne(document);
            }
//
//
//                DB.getCollection(collection)
//                        .insertOne(new org.bson.Document()
//                                .append("application",new org.bson.Document("started","true")));
//


//        }catch(Exception e){
//            logger.warn( e.getClass().getName() + ": " + e.getMessage() );
//        }
    }
}
