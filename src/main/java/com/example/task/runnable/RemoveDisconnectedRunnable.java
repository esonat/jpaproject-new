package com.example.task.runnable;

import com.example.connection.ClientManagerRemote;
import com.example.entities.Client;
import com.example.task.TaskManagerRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class RemoveDisconnectedRunnable implements Runnable {
    private Logger logger= LoggerFactory.getLogger(RemoveDisconnectedRunnable.class);

    private static String LNAME = "REMOVEDISCONNECTEDRUNNABLE";
    private static int DISCONNECT_TIME = 14;
    private String taskId;
    private List<Client> connectedClients;
    private ClientManagerRemote clientManager;
    private TaskManagerRemote taskManager;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public RemoveDisconnectedRunnable(String taskId) throws NamingException {
        InitialContext ctx=new InitialContext();
        clientManager=(ClientManagerRemote)ctx.lookup("java:global/Blog/ClientManager!com.example.connection.ClientManagerRemote");
        taskManager =(TaskManagerRemote)ctx.lookup("java:global/Blog/TaskManager!com.example.task.TaskManagerRemote");

        connectedClients=clientManager.getConnectedClients();
        this.taskId=taskId;
    }

    @Override
    public void run() {

    try {

       // logger.info("STARTED REMOVE DISCONNECTED");

        connectedClients = clientManager.getConnectedClients();
        boolean isEmpty = (connectedClients == null || connectedClients.size() == 0);

       //logger.info("CONNECTED CLIENTS IS EMPTY:"+isEmpty);


        if (!isEmpty) {

               // logger.info("CONNECTEDCLIENTS NOT EMPTY");

                Date now = new Date();
                List<String> removed = new ArrayList<String>();

                for (int index=0;index<connectedClients.size();index++) {
                    Client connectedClient = connectedClients.get(index);

                    if (connectedClient == null) {
                        logger.warn(LNAME + "NULL CLIENT");
                        return;
                    }

                    long difference = (now.getTime() - connectedClient.getLatestMessage().getTime()) / 1000;
                   /// logger.info("DIFFERENCE = "+difference);

                    if (difference > DISCONNECT_TIME) {
                    //    logger.info("DIFFERENCE > 14");
                        removed.add(connectedClient.getId());
                    }

              }

            clientManager.removeClients(removed);
            //logger.info(LNAME + "CLIENTS REMOVED");
            taskManager.updateTask(taskId);
        }


        }catch (Exception e){
            logger.error("Exception in task RemoveDisconnectedRunnable.TaskId:"+taskId);
        }
    }
}
