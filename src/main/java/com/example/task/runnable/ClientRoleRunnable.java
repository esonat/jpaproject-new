package com.example.task.runnable;

import com.example.connection.ClientManagerRemote;
import com.example.entities.Client;
import com.example.entities.ClientRole;
import com.example.entities.SentMessageType;
import com.example.entities.TaskType;
import com.example.message.MessageManagerRemote;
import com.example.task.TaskManagerRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.List;
import java.util.Random;

public class ClientRoleRunnable implements Runnable {

    private Logger logger= LoggerFactory.getLogger(ClientRoleRunnable.class);
    private List<Client> connectedClients;
    private static int CLIENT_ROLE_COUNT = 3;
    private static String LNAME = "CLIENTROLERUNNABLE";
    private static String MNAME="RUN";
    private String taskId;



    private ClientManagerRemote clientManager;
    private TaskManagerRemote taskManager;
    private MessageManagerRemote messageManager;

    public ClientRoleRunnable() throws NamingException{
        InitialContext ctx=new InitialContext();
        clientManager=(ClientManagerRemote)     ctx.lookup("java:global/Blog/ClientManager!com.example.connection.ClientManagerRemote");
        taskManager =(TaskManagerRemote)        ctx.lookup("java:global/Blog/TaskManager!com.example.task.TaskManagerRemote");
        messageManager=(MessageManagerRemote)   ctx.lookup("java:global/BlogEjb/MessageManager!com.example.message.MessageManagerRemote");

        connectedClients=clientManager.getConnectedClients();
    }
    public ClientRoleRunnable(String taskId) throws NamingException{
        InitialContext ctx=new InitialContext();
        clientManager=(ClientManagerRemote)ctx.lookup("java:global/Blog/ClientManager!com.example.connection.ClientManagerRemote");
        taskManager =(TaskManagerRemote)ctx.lookup("java:global/Blog/TaskManager!com.example.task.TaskManagerRemote");
        messageManager=(MessageManagerRemote)ctx.lookup("java:global/BlogEjb/MessageManager!com.example.message.MessageManagerRemote");

        connectedClients=clientManager.getConnectedClients();
        this.taskId=taskId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public void run() {

        try {
            connectedClients = clientManager.getConnectedClients();

             boolean isEmpty = (connectedClients == null || connectedClients.size() == 0);
             int size = connectedClients.size();

             if (!isEmpty) {
                 Random random = new Random();

                 int r_Id = 0;
                 int r_Role = 0;

                 try {
                     r_Id = random.nextInt(connectedClients.size());
                     r_Role = random.nextInt(CLIENT_ROLE_COUNT);

                 } catch (Exception e) {
                     logger.warn(LNAME + MNAME + "Error generating random numbers");
                     return;
                 }


                 if (connectedClients.size() == 1) r_Id = 0;

                 Client client = connectedClients.get(r_Id);

                 if (client == null) {
                     logger.warn(LNAME + MNAME + " Client with random id:" + r_Id + " does not exist");
                     return;
                 }

                 //connectedClients=null;
                 //connectedClients.clear();

                 String clientId;
                 if ((clientId = client.getId()) == null) {
                     logger.warn(LNAME + MNAME + " NULL CLIENT clientId:" + client.getId());
                     return;
                 }

                 String role;
                 if ((role = ClientRole.values()[r_Role].toString()) == null) {
                     logger.warn(LNAME + MNAME + " Client Role with index:" + r_Role + " does not exist");
                     return;
                 }

                 client.setClientRole(role);
                 clientManager.updateClient(client);
                 taskManager.updateTask(taskId);

                                /*
                                    SEND CLIENTROLEMESSAGE TO CLIENT
                                 */
                 messageManager.sendMessage(SentMessageType.CLIENTROLE, role, clientId);

                 if (role.equals(ClientRole.CATEGORY.toString())) {
                    /* ADD TASK TO TASK LIST */
                     Object message = messageManager.getMessage(TaskType.TASK_SENDCATEGORYLISTMESSAGE);
                     messageManager.sendMessage(SentMessageType.CATEGORYLIST, message, clientId);
                 }
             }

         }catch (Exception e){
            logger.error("Exception in task ClientRoleRunnable.TaskId:"+taskId);
         }
    }

}
