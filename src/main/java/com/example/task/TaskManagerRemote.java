package com.example.task;

import com.example.entities.Task;

import javax.ejb.Remote;
import java.util.List;

/**
 * Created by sonat on 19.09.2016.
 */

@Remote
public interface TaskManagerRemote {
    List<Task> getTasks();

    void setTasks(List<Task> tasks);

    void addTask(Task newTask);

    void updateTask(Task task);

    void updateTask(String taskId);

    void removeTask(int index);

    boolean canAddTask(Task newTask);

    int getTaskIndex(int taskId);

    Task getTaskByTaskId(String taskId);

    void Task_SendCategoryListMessage(Task task);

    void Task_SendClientRoleMessage(Task task);

    void Task_RemoveDisconnectedClients(Task task);

    void Task_AssignClientRole(Task task);
}
