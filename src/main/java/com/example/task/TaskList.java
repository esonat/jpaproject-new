//package com.example.task;
//
//import com.example.connection.ClientManager;
//import com.example.entities.*;
//import com.example.message.MessageManager;
//import com.example.service.BlogService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.ejb.EJB;
//import javax.ejb.Stateful;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.Random;
//import java.util.concurrent.Executors;
//import java.util.concurrent.ScheduledExecutorService;
//import java.util.concurrent.TimeUnit;
//
///**
// * Created by sonat on 20.08.2016.
// */
//
//@Stateful
//public class TaskList {
//    Logger logger= LoggerFactory.getLogger(TaskList.class);
//    private static final String LNAME="TASKLIST";
//    private static int CLIENT_ROLE_COUNT = 3;
//    private static int DISCONNECT_TIME = 14;
//
//    @EJB
//    MessageManager messageManager;
//    @EJB
//    ClientManager clientManager;
//    @EJB
//    BlogService blogService;
//    @EJB
//    TaskManager taskManager;
//
//    public void Task_SendCategoryListMessage(Task task) throws Exception{
//        try {
//
//            List<Category> categories = blogService.getAllCategories();
//            List<String> categoryNames=new ArrayList<String>();
//
//            if(categories==null){
//                logger.info(LNAME+"There are no categories");
//                return;
//            }
//
//            for(Category category:categories){
//                categoryNames.add(category.getName());
//            }
//
//            if(task.getClients()==null || task.getClients().size()==0){
//                logger.error(LNAME+"Task_SendCategoryListMessage.Task doesn't have clients");
//                throw new RuntimeException(LNAME+"Task_SendCategoryListMessage.Task doesn't have clients");
//            }
//
//            logger.info(LNAME+"Task_SendCategoryListMessage.Clients size ="+task.getClients().size());
//
//            for (RestClient receiver : task.getClients()) {
//                    messageManager.sendMessage(SentMessageType.CATEGORYLIST,categoryNames, receiver.getId());
//                    logger.info("SENDCATEGORYLISTMESSAGE ClientId:"+receiver.getId());
//            }
//
//            task.setEndTime(new Date());
//            task.setCompleted(true);
//
//            blogService.updateTask(task);
//
//        }catch (Exception e){
//            logger.warn(LNAME+"Task_SendCategoryListMessage:"+e.getMessage()+e.getCause());
//            throw new RuntimeException(LNAME+"Task_SendCategoryListMessage:"+e.getMessage()+e.getCause());
//        }
//    }
//
//    public void Task_SendClientRoleMessage(Task task) throws Exception{
//        try {
//
//            if(task.getClients()==null || task.getClients().size()==0){
//                logger.error(LNAME+"Task_SendClientRoleMessage.Task doesn't have clients");
//
//                throw new RuntimeException(LNAME+"Task_SendClientRoleMessage.Task doesn't have clients");
//            }
//
//            for (RestClient receiver : task.getClients()) {
//                messageManager.sendMessage(SentMessageType.CLIENTROLE,task.getMessage(), receiver.getId());
//                logger.info(LNAME+"Task_SendClientRoleMessage"+task.getMessage()+receiver.getId());
//            }
//
//            task.setEndTime(new Date());
//            task.setCompleted(true);
//
//            blogService.updateTask(task);
//
//        }catch (Exception e){
//            logger.warn(LNAME+"Task_SendClientRoleMessage:"+e.getMessage()+e.getCause());
//            throw new RuntimeException(LNAME+"Task_SendClientRoleMessage"+e.getMessage()+e.getCause());
//        }
//    }
//
//    public void Task_RemoveDisconnectedClients() throws ArrayIndexOutOfBoundsException,Exception{
//        try {
//            Runnable runnable = new Runnable() {
//                public void run() {
//                    try {
//                        List<RestClient> connectedClients= clientManager.getConnectedClients();
//
//                        if (connectedClients != null && connectedClients.size() > 0) {
//
//                            Date now = new Date();
//                            List<String> removed = new ArrayList<String>();
//
//                            for (int index = 0; index < connectedClients.size(); index++) {
//                                RestClient connectedClient = connectedClients.get(index);
//
//                                if (connectedClient == null) {
//                                    logger.error(LNAME + "TaskRemoveDisconnectedClients.RestClient with index:" + index + "not found in connectedClients");
//                                    throw new RuntimeException(LNAME + "TaskRemoveDisconnectedClients.RestClient with index:" + index + "not found in connectedClients");
//                                }
//
//                                if (connectedClient.getLatestMessage() == null) {
//                                    connectedClient.setLatestMessage(new Date());
//                                }
//
//                                long difference = (now.getTime() - connectedClient.getLatestMessage().getTime()) / 1000;
//                                if (difference > DISCONNECT_TIME) removed.add(connectedClient.getId());
//                            }
//
//                            clientManager.removeClients(removed);
//                            clientManager.updateTask(TaskType.TASK_REMOVEDISCONNECTED.toString());
//                            removed.clear();
//                        }
//                    } catch (ArrayIndexOutOfBoundsException e){
//                        logger.warn(LNAME+"Task_RemoveDisconnectedClients "+ e.getCause() + e.getMessage());
//                        throw new RuntimeException(LNAME+"Task_RemoveDisconnectedClients ",e);
//                    }catch (Exception e) {
//                        logger.warn(LNAME+"Task_RemoveDisconnectedClients "+ e.getCause() + e.getMessage());
//                        throw new RuntimeException(LNAME+"Task_RemoveDisconnectedClients ",e);
//                    }
//                }
//            };
//
//            ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
//            service.scheduleAtFixedRate(runnable, 0, 15, TimeUnit.SECONDS);
//
//        }catch (Exception e){
//            logger.error(LNAME+"RemoveDisconnectedClients");
//            throw new RuntimeException(LNAME+"RemoveDisconnectedClients");
//        }
//    }
//
//
//    public void Task_AssignClientRole() throws ArrayIndexOutOfBoundsException,Exception {
//        try {
//            Runnable runnable = new Runnable() {
//                public void run() {
//                    try {
//
//                        List<RestClient> connectedClients= clientManager.getConnectedClients();
//
//                        if (connectedClients != null && connectedClients.size() > 0) {
//                            Random random = new Random();
//
//                            int r_Id = random.nextInt(connectedClients.size());
//                            int r_Role = random.nextInt(CLIENT_ROLE_COUNT);
//
//                            if (connectedClients.size() == 1) r_Id = 0;
//
//
//                            RestClient client = connectedClients.get(r_Id);
//                            if (client == null) {
//                                logger.warn(LNAME + "Task_AssignClientRole.RestClient with random id:" + r_Id + " does not exist");
//                                throw new RuntimeException(LNAME + "Task_AssignClientRole.RestClient with random id:" + r_Id + " does not exist");
//                            }
//
//                            String clientId = client.getId();
//                            String role = ClientRole.values()[r_Role].toString();
//
//                            if (role == null) {
//                                logger.warn(LNAME + "Task_AssignClientRole.Role with random id:" + r_Role + " does not exist");
//                                throw new RuntimeException(LNAME + "Task_AssignClientRole.Role with random id:" + r_Role + " does not exist");
//                            }
//
//                            client.setClientRole(role);
//
//                            clientManager.updateClient(client.getId());
//                            clientManager.updateTask(TaskType.TASK_ASSIGNROLE.toString());
//
//                                /*
//                                    SEND MESSAGE TO CLIENT
//                                 */
//
//                            Task roleTask = new Task();
//                            roleTask.setType(TaskType.TASK_SENDCLIENTROLEMESSAGE.toString());
//                            roleTask.setCategory(TaskCategory.MESSAGETASK.toString());
//                            roleTask.setLongRunning(false);
//
//                            RestClient receiver = blogService.getClientById(client.getId());
//                            if (receiver == null) {
//                                logger.error(LNAME + "Task_AssignClientRole.Role message receiver with id:" + client.getId() + "not found");
//                                throw new RuntimeException(LNAME + "Task_AssignClientRole.Role message receiver with id:" + client.getId() + "not found");
//                            }
//
//                            List<RestClient> list = new ArrayList<RestClient>();
//                            list.add(client);
//                            roleTask.setClients(list);
//                            roleTask.setMessage(ClientRole.values()[r_Role].toString());
//                            roleTask.setMessageType(SentMessageType.CLIENTROLE);
//
//                            taskManager.addTask(roleTask);
//
//                            if (role.equals(ClientRole.CATEGORY.toString())) {
//                                Task taskCategory = new Task();
//                                taskCategory.setType(TaskType.TASK_SENDCATEGORYLISTMESSAGE.toString());
//                                taskCategory.setCategory(TaskCategory.MESSAGETASK.toString());
//                                taskCategory.setLongRunning(false);
//
//                                List<RestClient> clients = new ArrayList<RestClient>();
//                                RestClient categoryReceiver = blogService.getClientById(clientId);
//
//                                if (categoryReceiver == null) {
//                                    logger.error(LNAME + "Task_AssignClientRole.CategoryListMessage receiver with id:" + clientId + "not found");
//                                    throw new RuntimeException("Task_AssignClientRole.CategoryListMessage receiver with id:" + clientId + " not found");
//                                }
//
//                                clients.add(categoryReceiver);
//
//                                if (taskCategory.getClients() == null) taskCategory.setClients(new ArrayList<RestClient>());
//                                if (categoryReceiver.getTasks() == null) categoryReceiver.setTasks(new ArrayList<Task>());
//
//                                List<RestClient> receiverList=new ArrayList<RestClient>();
//                                receiverList.add(categoryReceiver);
//
//                                List<Task> taskList=new ArrayList<Task>();
//                                taskCategory.setClients(receiverList);
//                                categoryReceiver.setTasks(taskList);
//
//                                logger.info("TASK_SENDCATEGORYLISTMESSAGE: SET TASK CLIENTS");
//                                logger.info("TASK_SENDCATEGORYLISTMESSAGE: SET CLIENT TASKS");
//
//                                taskCategory.setMessage(null);
//                                taskCategory.setMessageType(SentMessageType.CATEGORYLIST);
//
//                                taskManager.addTask(taskCategory);
//                            }
//                            //MessageProducer.sendMessage(ClientRole.values()[r_Role],client.getId());
//
//                                /*
//                                    MESSAGE SENT
//                                 */
//                        }
//                    }catch (ArrayIndexOutOfBoundsException e){
//                        logger.error(LNAME+"Task_AssignClientRole "+e.getMessage()+e.getCause());
//                        throw new RuntimeException(LNAME+"Task_AssignClientRole "+e.getMessage()+e.getCause());
//                    } catch (Exception e) {
//                        logger.error(LNAME+"Task_AssignClientRole "+e.getMessage()+e.getCause());
//                        throw new RuntimeException(LNAME+"Task_AssignClientRole "+e.getMessage()+e.getCause());
//                    }
//                }
//            };
//
//            ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
//            service.scheduleAtFixedRate(runnable, 0, 10, TimeUnit.SECONDS);
//
//        }catch (Exception e){
//            logger.error(LNAME+"Task_AssignClientRole "+e.getMessage()+e.getCause());
//            throw new RuntimeException(LNAME+"Task_AssignClientRole ",e);
//        }
//    }
//}
