package com.example.task;

import com.example.connection.ClientManagerRemote;
import com.example.entities.Category;
import com.example.entities.Task;
import com.example.entities.TaskType;
import com.example.exception.RepositoryException;
import com.example.message.MessageManagerRemote;
import com.example.service.BlogServiceRemote;
import com.example.task.runnable.ClientRoleRunnable;
import com.example.task.runnable.RemoveDisconnectedRunnable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


@Singleton
public class TaskManager implements TaskManagerRemote {

    private static Logger logger = LoggerFactory.getLogger(TaskManager.class);
    private List<Task> tasks;
    private static int CLIENT_ROLE_COUNT = 3;
    private static int DISCONNECT_TIME = 14;

    @EJB
    BlogServiceRemote blogService;

    @EJB
    ClientManagerRemote clientManager;

    @EJB
    MessageManagerRemote messageManager;

    @PostConstruct
    public void init() {
        tasks = new ArrayList<Task>();
    }

    @Override
    public List<Task> getTasks() {
        return tasks;
    }

    @Override
    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }


    @Override
    public void addTask(Task newTask) {

        if (canAddTask(newTask)) {

            //newTask.setTaskId(UUID.randomUUID());
            newTask.setCompleted(false);
            newTask.setLatestActivity(null);
            newTask.setEndTime(null);
            newTask.setStartTime(new Date());

            try {
                tasks.add(newTask);
                blogService.addTask(newTask);
                //blogService.addTaskAndClients(newTask);
            }catch (RepositoryException e){
                logger.error("Exception while adding task:"+e.getMessage());
            }

            if (newTask.getType().equals(TaskType.TASK_ASSIGNROLE.toString())) {
                Task_AssignClientRole(newTask);
            }

            if (newTask.getType().equals(TaskType.TASK_REMOVEDISCONNECTED.toString())) {
                Task_RemoveDisconnectedClients(newTask);
            }

            if (newTask.getType().equals(TaskType.TASK_SENDCATEGORYLISTMESSAGE.toString())) {
                Task_SendCategoryListMessage(newTask);
            }

            if (newTask.getType().equals(TaskType.TASK_SENDCLIENTROLEMESSAGE.toString())) {
                Task_SendClientRoleMessage(newTask);
            }

        }
    }

    @Override
    public void updateTask(Task task){
        try {
            task.setLatestActivity(new Date());
            blogService.updateTask(task);

        }catch (RepositoryException e){
            logger.error(e.getMessage());
        }
    }

    @Override
    public void updateTask(String taskId){
        try{
            Task task=getTaskByTaskId(taskId);
            task.setLatestActivity(new Date());
            blogService.updateTask(task);
        }catch (RepositoryException e){
            logger.error(e.getMessage());
        }
    }


    @Override
    public void removeTask(int index) {
        try {
            tasks.remove(index);
            Task removed = tasks.get(index);

            blogService.removeTask(removed);

        }catch (NullPointerException e) {
            logger.error(e.getMessage());
        } catch (ArrayIndexOutOfBoundsException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error("Exception while removing task");
        }
    }

    @Override
    public boolean canAddTask(Task newTask){

        if(tasks==null || tasks.size()==0) return true;

        for (Task task : tasks) {
            try {
                if (!task.noConflict(newTask)) return false;

            }catch (Exception e){
                logger.error("Exception while checking task for conflict");
                return false;
            }
        }

        return true;
    }

    @Override
    public int getTaskIndex(int taskId){
        try {
                if (tasks != null && tasks.size() > 0) {
                    for (Task task : tasks) {
                        if (task.getId() == taskId) return taskId;
                    }
                }

        }catch (Exception e) {
            logger.error("Exception while getting task index.TaskId:"+taskId);
        }
        return -1;
    }

    @Override
    public Task getTaskByTaskId(String taskId){
        try {
            if (tasks == null || tasks.size() == 0) return null;

            for (Task task : tasks) {
                if (task.getTaskId().toString().equals(taskId)) return task;
            }
        }catch (Exception e){
            logger.error("Exception while getting task by task id.TaskId:"+taskId);
        }
        return null;
    }





    @Override
    public void Task_SendCategoryListMessage(Task task) {
            List<Category> categories=null;

            try {
                categories = blogService.getAllCategories();
            }catch (RepositoryException e){
                logger.error(e.getMessage());
            }

            List<String> categoryNames=new ArrayList<String>();

            if(categories==null) return;

            for(Category category:categories){
                categoryNames.add(category.getName());
            }

            task.setEndTime(new Date());
            task.setCompleted(true);

            updateTask(task);
    }

    @Override
    public void Task_SendClientRoleMessage(Task task){

            task.setEndTime(new Date());
            task.setCompleted(true);

            updateTask(task);
    }

    @Override
    public void Task_RemoveDisconnectedClients(Task task) {
        try {
            Runnable runnable = new RemoveDisconnectedRunnable(task.getTaskId().toString());

            ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
            service.scheduleAtFixedRate(runnable, 0, 15, TimeUnit.SECONDS);

        } catch (NamingException e) {
            logger.error("Naming Exception while starting remove disconnected task"+e.getMessage());
        } catch (Exception e) {
            logger.error("Exception while starting remove disconnected task");
        }
    }

    @Override
    public void Task_AssignClientRole(Task task) {
        try {
            Runnable runnable = new ClientRoleRunnable(task.getTaskId().toString());

            ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
            service.scheduleAtFixedRate(runnable, 0, 10, TimeUnit.SECONDS);

        }catch(NamingException e){
            logger.error("Naming Exception while starting client role task:"+e.getMessage());
        }catch (Exception e){
            logger.error("Exception while starting client role task");
        }
    }

}
