package com.example.message;

import com.example.entities.Category;
import com.example.entities.SentMessageType;
import com.example.entities.TaskType;
import com.example.exception.GeneralException;
import com.example.exception.RepositoryException;
import com.example.service.BlogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Singleton
public class MessageManager implements MessageManagerRemote {
    Logger logger= LoggerFactory.getLogger(MessageManager.class);

    @EJB
    BlogService blogService;

    @Override
    public boolean sendMessage(SentMessageType type, Object object, String clientId){
       try{
                MessageProducer.sendMessage(type,object, clientId);
                return true;

       }catch (IOException e) {
           logger.error("IO Exception while sending message:" + e.getMessage());
       }catch (TimeoutException e){
           logger.error("Timeout Exception while sending message"+e.getMessage());
       }catch (GeneralException e){
            logger.error("General exception while sending message");
       }finally{
           return false;
       }
    }

    public Object getMessage(TaskType type){
        try {
            if (type.equals(TaskType.TASK_SENDCATEGORYLISTMESSAGE)) {

                List<Category> categories = null;

                try {
                    categories = blogService.getAllCategories();

                } catch (RepositoryException e) {
                    logger.error(e.getMessage());
                }

                List<String> categoryNames = new ArrayList<String>();

                if (categories == null) return null;

                for (Category category : categories) {
                    categoryNames.add(category.getName());
                }

                return categoryNames;
            }
        }catch (Exception e){
            logger.error("Exception in MessageManager getMessage method.Type:"+type);
        }
        return null;
    }

//    public boolean sendCategoryListMessage(SentMessageType type,Object object,List<String> clientIds){
//        try {
//                for (String clientId : clientIds){
//                    MessageProducer.sendMessage(type,object, clientId);
//                }
//        }
//        catch (IOException e){
//                logger.warn("MESSAGE MANAGER-SENDCATEGORYLIST"+e.getMessage());
//                return false;
//        }catch (Exception e){
//                logger.warn("MESSAGEMANAGER -SENDCATEGORYLIST"+e.getMessage());
//                return false;
//        }finally {
//            return true;
//        }
//    }
//
//    public boolean sendUserListMessage(Object object,List<String> clientIds){
//        try {
//            for (String clientId : clientIds){
//                MessageProducer.sendMessage(object, clientId);
//            }
//        }
//        catch (IOException e){
//            logger.warn("MESSAGE MANAGER-SENDCATEGORYLIST"+e.getMessage());
//            return false;
//        }catch (Exception e){
//            logger.warn("MESSAGEMANAGER -SENDCATEGORYLIST"+e.getMessage());
//            return false;
//        }finally {
//            return true;
//        }
//    }


}
