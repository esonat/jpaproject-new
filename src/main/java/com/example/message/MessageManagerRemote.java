package com.example.message;

import com.example.entities.SentMessageType;
import com.example.entities.TaskType;

import javax.ejb.Remote;

/**
 * Created by sonat on 25.08.2016.
 */
@Remote
public interface MessageManagerRemote {
    boolean sendMessage(SentMessageType type, Object object, String clientId);

    Object getMessage(TaskType type);
}
