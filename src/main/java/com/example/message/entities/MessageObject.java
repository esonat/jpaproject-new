package com.example.message.entities;


import java.io.Serializable;

public class MessageObject implements Serializable {
    String type;
    Object message;


    public MessageObject(){}

    public MessageObject(String type,Object message){
        this.type=type;
        this.message=message;
    }
    public String getType(){
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }
}
