package com.example.message;

import com.example.entities.SentMessageType;
import com.example.exception.GeneralException;
import com.example.message.entities.MessageObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


public class MessageProducer {
    public static Logger logger= LoggerFactory.getLogger(MessageProducer.class);

    public static void sendMessage(SentMessageType type, Object object, String clientId) throws IOException,
    TimeoutException,
    GeneralException{

        try {
                Producer producer = new Producer(clientId);

                MessageObject message=null;
              //  Mapper mapper=InitMapper.getMapper();

                if(type.equals(SentMessageType.CATEGORY)){
                    message=new MessageObject("Category",object);
                    producer.sendMessage(message);
                }
                if(type.equals(SentMessageType.USER)){
                    message = new MessageObject("User",object);
                    producer.sendMessage(message);
                }
                if(type.equals(SentMessageType.CLIENTROLE)){
                    message=new MessageObject("ClientRole",object);
                    producer.sendMessage(message);
                }
                if(type.equals(SentMessageType.CATEGORYLIST)){
                    message=new MessageObject("CategoryList",object);
                    producer.sendMessage(message);
                }

        }catch (IOException e){
            throw new IOException("IOException while sending message");
        }catch (TimeoutException e) {
            throw new TimeoutException("Timeout exception while sending message:"+e.getMessage());
        }catch (Exception e){
            throw new GeneralException("General exception while sending message");
        }
    }
}