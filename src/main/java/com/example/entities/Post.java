package com.example.entities;

import com.example.validation.PostUsername;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.glassfish.jersey.linking.Binding;
import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLink.Style;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.core.Link;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name="Post.findAll",
            query="SELECT p FROM Post p"),
        @NamedQuery(name="Post.findComments",
            query="select p.comments from Post p"),
        @NamedQuery(name="Post.findByUserName",
            query="SELECT p FROM Post p WHERE p.user.userinfo.name = :name")
})
public class Post implements Serializable {
    private static final long serialVersionUID = 4535704024413259871L;
    @JsonProperty
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonIgnore
    @Version
    private int version;

    @JsonProperty
    @NotNull
    @Size(min = 3)
    @Column(name="TEXT",length = 255,unique = true)
    private String text;

    @JsonProperty
    @Column(name="DATETIME")
    @Temporal(TemporalType.TIME)
    private Date datetime;

    @JsonProperty
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.PERSIST)
    @JoinColumn(name="DOCUMENT_ID")
    private Document document;

    @JsonProperty
    @Size(max=2,message = "Post can not have more than 2 comments")
    @OneToMany(mappedBy = "post",cascade = CascadeType.REMOVE,fetch = FetchType.EAGER)
    @OrderBy("depth ASC")
    private List<Comment> comments;

    @JsonProperty
    @PostUsername
    @ManyToOne
    @JoinColumn(name="USER_ID")
    private User user;

    @JsonProperty
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    @JoinColumn(name="CATEGORY_ID")
    private Category category;

//    @XmlElement(name="link")
//    @XmlJavaTypeAdapter(XmlAdapter.class)
//    public Link getUserLink(){
//        Link userLink=Link.fromUri("{id}/user")
//                .rel("user").build(id);
//        return userLink;
//    }


    @JsonIgnore
    @Transient
    @InjectLink(
            value="{id}/user",
            style = Style.RELATIVE_PATH,
            bindings = @Binding(name="id",value="${instance.id}"),
            rel="user"
    )
    @XmlJavaTypeAdapter(XmlAdapter.class)
    @XmlElement(name="link")
    Link link;

    public Post(){
        comments=new ArrayList<Comment>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
