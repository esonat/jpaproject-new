package com.example.entities;

import java.io.Serializable;

/**
 * Created by sonat on 03.09.2016.
 */
public class SystemInfo implements Serializable {

    private static final long serialVersionUID = -548770657647684375L;
    private String hostName;
    private String processor;
    private String serverName;
    private int userSessions;
    private int threads;
    SystemInfo SystemInfo = null;

    private SystemInfo() {
        initWithDummyValues();
    }

    public static SystemInfo getInstance() {
        return new SystemInfo();
    }

    private void initWithDummyValues() {
        hostName = "localhost";
        processor = "Intel";
        serverName = "XXX";
        userSessions = 123233;
        threads = 334235235;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public int getUserSessions() {
        return userSessions;
    }

    public void setUserSessions(int userSessions) {
        this.userSessions = userSessions;
    }

    public int getThreads() {
        return threads;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }
}
