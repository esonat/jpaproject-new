package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="CLIENT")
public class Client implements Serializable{

    private static final long serialVersionUID = -3000402290034078104L;

    @Id
    private String id;

    @Column(name="PORT")
    private int port;

    @Column(name="STATUS")
    private boolean status;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="USER_ID")
    private User user;

    @Column(name="ROLE")
    private String clientRole;

    @Temporal(TemporalType.TIME)
    @Column(name="LAST_ACTIVITY_TIME")
    private Date latestActivity;

    @Temporal(TemporalType.TIME)
    @Column(name="LATEST_MESSAGE")
    private Date latestMessage;

    //@ManyToMany(mappedBy = "clients")
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "CLIENT_TASKS",
            joinColumns = { @JoinColumn(name = "CLIENT_ID") },
            inverseJoinColumns = { @JoinColumn(name = "TASK_ID") })
    private List<Task> tasks;

    public Client() {
        tasks=new ArrayList<Task>();
    }

    public Client(String id,int port,boolean status,User user,
                  String clientRole,Date latestActivity,Date latestMessage){
        this.id=id;
        this.port=port;
        this.status=status;
        this.user=user;
        this.clientRole=clientRole;
        this.latestActivity=latestActivity;
        this.latestMessage=latestMessage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getLatestActivity() {
        return latestActivity;
    }

    public void setLatestActivity(Date latestActivity) {
        this.latestActivity = latestActivity;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getClientRole() {
        return clientRole;
    }

    public void setClientRole(String clientRole) {
        this.clientRole = clientRole;
    }

    public Date getLatestMessage() {
        return latestMessage;
    }

    public void setLatestMessage(Date latestMessage) {
        this.latestMessage = latestMessage;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public boolean isStatus() {
        return status;
    }

    @Override
    public String toString(){
        return "Id:"+getId()+" port:"+getPort()
                + "latestActivity:"+getLatestActivity()
                +" status:"+getStatus()
                +" username:"+getUser().getUserLoginInfo().getUsername()
                +" role:"+getClientRole()+" latestMessage:"+getLatestMessage();
    }
}
