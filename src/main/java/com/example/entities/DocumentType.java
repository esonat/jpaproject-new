package com.example.entities;

import java.io.Serializable;

/**
 * Created by sonat on 12.08.2016.
 */


public enum DocumentType implements Serializable{

    TXT,
    JPG
}
