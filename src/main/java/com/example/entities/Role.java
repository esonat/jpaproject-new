package com.example.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonat on 09.08.2016.
 */
@Entity
public class Role implements Serializable{
    private static final long serialVersionUID = 6092198373388527556L;

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonIgnore
    @Version
    private int version;

    @Column(name="ROLE_NAME")
    private String name;


//    @ManyToMany
//    @JoinTable(name="USER_ROLES",
//        joinColumns =
//            @JoinColumn(name="ROLE_ID",referencedColumnName = "ID"),
//        inverseJoinColumns =
//            @JoinColumn(name="USER_ID",referencedColumnName = "ID"))

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "role",cascade = CascadeType.PERSIST)
    private List<UserRole> userRoles=new ArrayList<UserRole>();

    public Role(){
        userRoles=new ArrayList<UserRole>();
    }

    public Role(String name){
        this.name=name;
        userRoles=new ArrayList<UserRole>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<UserRole> users) {
        this.userRoles= users;
    }
}
