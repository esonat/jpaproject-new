package com.example.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by sonat on 12.08.2016.
 */
@Entity
@Table(name="TESTITEM")
public class TestItem implements Serializable {

    private static final long serialVersionUID = 2581319158932126670L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="TEXT")
    private String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
