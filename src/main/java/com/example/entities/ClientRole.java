package com.example.entities;

import java.io.Serializable;

/**
 * Created by sonat on 14.08.2016.
 */
public enum ClientRole implements Serializable{

    POST,
    CATEGORY
};
