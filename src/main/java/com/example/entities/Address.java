package com.example.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import java.io.Serializable;

/**
 * Created by sonat on 08.08.2016.
 */

@Embeddable
public class Address implements Serializable{

    private static final long serialVersionUID = -5275943854941654607L;
    @OneToOne
    private City city;

    private String street;

    private String state;

    @Column(name="ZIP_CODE")
    private String zip;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
