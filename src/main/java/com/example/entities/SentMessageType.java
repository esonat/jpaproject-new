package com.example.entities;

import java.io.Serializable;

/**
 * Created by sonat on 20.08.2016.
 */
public enum SentMessageType implements Serializable {
    CATEGORYLIST,
    USERLIST,
    STRING,
    STRINGLIST,
    CLIENTROLE,
    CATEGORY,
    USER
}
