package com.example.entities;

import java.io.Serializable;

/**
 * Created by sonat on 17.08.2016.
 */
public enum ReceivedMessageType implements Serializable {
    HEARTBEAT,
    POST,
    CATEGORY
}
