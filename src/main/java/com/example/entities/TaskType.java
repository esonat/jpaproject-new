package com.example.entities;

import java.io.Serializable;

/**
 * Created by sonat on 15.08.2016.
 */
public enum TaskType implements Serializable {
    TASK_ASSIGNROLE,        /*Assign client roles*/
    TASK_REMOVEDISCONNECTED, /*Remove disconnected clients*/
    TASK_SENDCLIENTROLEMESSAGE,
    TASK_SENDCATEGORYLISTMESSAGE,
    TASK_SENDUSERLISTMESSAGE
}
