package com.example.entities;

import java.io.Serializable;

/**
 * Created by sonat on 18.08.2016.
 */
public enum TaskCategory implements Serializable {
    SYSTEMTASK,
    CLIENTTASK,
    MESSAGETASK,
}
