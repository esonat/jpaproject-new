package com.example.entities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name="TASK")
public class Task implements Serializable {
    private static final long serialVersionUID = -7602123462674281825L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="TASKID")
    private UUID taskId;

    @Column(name="TYPE")
    private String type;

    @Column(name="CATEGORY")
    private String category;

    @Temporal(TemporalType.TIME)
    private Date startTime;

    @Temporal(TemporalType.TIME)
    @Column(name="LAST_ACTIVITY_TIME")
    private Date latestActivity;

    @Column(name="LONG_RUNNING")
    private boolean longRunning;

    @Temporal(TemporalType.TIME)
    @Column(name="ENDTIME")
    private Date endTime;

    @Column(name="COMPLETED")
    private boolean completed;

//    @JoinTable(name="CLIENT_TASKS",
//            inverseJoinColumns={
//                    @JoinColumn(name = "TASK_ID", referencedColumnName = "CLIENT_ID")
//            })
//    @ManyToMany(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST,CascadeType.REFRESH})

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "CLIENT_TASKS",
            joinColumns = { @JoinColumn(name = "TASK_ID") },
            inverseJoinColumns = { @JoinColumn(name = "CLIENT_ID") })
    private List<Client> clients;

    @Transient
    private Object message;

    @Transient
    public SentMessageType messageType;

    public Task(){
        clients=new ArrayList<Client>();
    }

    public Task(String type, String category, Date startTime, Date latestActivity, boolean longRunning, Date endTime, boolean completed, List<Client> clients, Object message, SentMessageType messageType) {
        this.type = type;
        this.category = category;
        this.startTime = startTime;
        this.latestActivity = latestActivity;
        this.longRunning = longRunning;
        this.endTime = endTime;
        this.completed = completed;
        this.clients = clients;
        this.message = message;
        this.messageType = messageType;
    }

    public void start(){

    }
    //    @Embedded
//    private SentMessage message;

//    @JoinTable(name="CLIENT_TASKS",
//            inverseJoinColumns={
//                @JoinColumn(name = "TASK_ID", referencedColumnName = "CLIENT_ID")
//            })
//    @ManyToMany(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST,CascadeType.REFRESH})
//    private List<RestClient> clients;
//
//    @Transient
//    private List<String> message;
//

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getTaskId() {
        return taskId;
    }

    public void setTaskId(UUID taskId) {
        this.taskId = taskId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getLatestActivity() {
        return latestActivity;
    }

    public void setLatestActivity(Date latestActivity) {
        this.latestActivity = latestActivity;
    }

    public boolean isLongRunning() {
        return longRunning;
    }

    public void setLongRunning(boolean longRunning) {
        this.longRunning = longRunning;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public SentMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(SentMessageType messageType) {
        this.messageType = messageType;
    }

    public boolean noConflict(Task task){
        String oldCategory      =this.getCategory();
        String newCategory      =task.getCategory();
        String oldType          =this.getType();
        String newType          =task.getType();
        boolean oldCompleted    =this.isCompleted();
        boolean newCompleted=task.isCompleted();

        if (!oldCategory.equals(newCategory)) return true;
        if (!oldType.equals(newType))         return true;
        if (oldCompleted)                     return true;

        if (oldCategory.equals(TaskCategory.MESSAGETASK.toString())) {

            if(oldType.equals(newType)){
                    List<Client> oldClients = this.getClients();
                    List<Client> newClients = task.getClients();

                /*CATEGORYLIST CATEGORYLIST */
//                    return true;
//                if((oldClients==null || oldClients.size()==0) && (newClients==null || newClients.size()==0))
                boolean contains=false;

                if(newClients!=null && oldClients!=null){

                    for(Client newClient:newClients){
                        for(Client oldClient:oldClients){
                            if(oldClient.getId().equals(newClient.getId())) contains=true;
                        }
                    }

                    if(!contains) return true;
                }else{

                    if(newClients==null) return false;
                    if(newClients!=null) return true;
                }

                /* CATEGORYLIST CLIENTROLE */
            }else if(!oldType.equals(newType)){
                return true;
            }
         }

        return false;
    }

    @Override
    public String toString(){
        return "Task id:"+this.getId()+",type:"+this.getType()+",category:"+this.getCategory()+
                ",starttime:"+this.getStartTime()+",endtime:"+this.getEndTime()+"completed:"+this.isCompleted()+
                ",longrunning:"+this.isLongRunning();
    }
}

