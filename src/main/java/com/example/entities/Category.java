package com.example.entities;

import com.example.validation.CategoryName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name="Category.findByName",
        query = "SELECT c FROM Category c WHERE c.name = :name")
})
public class Category implements Serializable{
    private static final long serialVersionUID = 118993491381181460L;
    @JsonProperty
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Version
    private int version;


    @CategoryName(message = "Category name must be unique")
    @JsonProperty
    @Pattern(regexp = "^[A-Za-z0-9]*$",message = "Invalid category name")
    @Size(min=3,max=15,message = "Category name must be between 3 and 15 characters")
    @Column(name="CATEGORY_NAME")
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "category",fetch = FetchType.EAGER,cascade ={CascadeType.REMOVE,CascadeType.REFRESH,CascadeType.MERGE,CascadeType.PERSIST})
    private List<Post> posts;


    public Category(){
        posts=new ArrayList<Post>();
    }

    public Category(String name){
        this.name=name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
