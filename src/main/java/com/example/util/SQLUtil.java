package com.example.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLUtil {
    private static Logger logger= LoggerFactory.getLogger(SQLUtil.class);


    public static boolean executeSQLScript(){
        try {
            //   Process process = new ProcessBuilder("mysql --user=root --password=sonat < /home/sonat/Documents/JPAProjectConfig/JPAProjectStartup.sql").start();
            String[] commandArray = {
                    "/home/sonat/Documents/JPAProjectConfig/startupsql.sh"
            };

            Process p = Runtime.getRuntime().exec(commandArray);
            int status = p.waitFor();
        } catch (Exception e) {
            logger.warn(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean executeStartupScript() {
        try {
            //   Process process = new ProcessBuilder("mysql --user=root --password=sonat < /home/sonat/Documents/JPAProjectConfig/JPAProjectStartup.sql").start();
            String[] commandArray = {
                    "/home/sonat/Documents/JPAProjectConfig/startup.sh"
            };

            Process p = Runtime.getRuntime().exec(commandArray);
            int status = p.waitFor();
        } catch (Exception e) {
            logger.warn(e.getMessage());
            return false;
        }
        return true;
    }

    public static boolean executeShutdownScript(){
        try {
              String[] commandArray = {
                    "/home/sonat/Documents/JPAProjectConfig/shutdown.sh"
            };

            Process p = Runtime.getRuntime().exec(commandArray);
            int status = p.waitFor();
        } catch (Exception e) {
            logger.warn(e.getMessage());
            return false;
        }
        return true;
    }
}
