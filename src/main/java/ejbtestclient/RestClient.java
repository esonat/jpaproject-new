package ejbtestclient;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by sonat on 09.08.2016.
 */
public class RestClient {

    public static void main(String[] args){
        Client client= ClientBuilder.newClient();
        WebTarget target=client.target("http://localhost:8080/Blog/services/post");

        MultivaluedMap<String,String> postForm=
                new MultivaluedHashMap<String,String>();
        postForm.add("text","post123");
        postForm.add("username","engin");
        postForm.add("category","Java");

        String responseData=target.request()
                .post(Entity.form(postForm),String.class);
        System.out.println(responseData);

    }
}
