package com.example.rest.resource;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlMetadataHandler;
import org.dbunit.operation.DatabaseOperation;
import org.hamcrest.Matchers;
import org.junit.*;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

/**
 * Created by sonat on 16.09.2016.
 */
public class PostResourceTest {
    private final String jdbcURL="jdbc:mysql://localhost:3306/JPAProject?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final String mysql_driver="com.mysql.cj.jdbc.Driver";
    private final String mysql_username="root";
    private final String mysql_password="sonat";

    private final int POST_COUNT=3;

    @BeforeClass
    public static void createSchema() throws Exception{

    }

    private IDataSet readDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new File("full.xml"));
    }

    private void cleanlyInsertDataset(IDataSet dataSet) throws Exception{

        IDatabaseTester databaseTester=new JdbcDatabaseTester(mysql_driver,jdbcURL,mysql_username,mysql_password);
        databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
    }


    @Before
    public void setup() throws Exception{
        Class.forName (mysql_driver).newInstance ();
        Connection conn = DriverManager.getConnection (jdbcURL, "root", "sonat");

        IDatabaseConnection dbUnitConn = new DatabaseConnection(conn,"JPAProject");
        DatabaseConfig dbCfg = dbUnitConn.getConfig();
        dbCfg.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);
        dbCfg.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
        dbCfg.setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());

        IDataSet dataSet = new FlatXmlDataSetBuilder().build(new File("full.xml"));
        DatabaseOperation.CLEAN_INSERT.execute(dbUnitConn,dataSet);

        RestAssured.baseURI="http://localhost:8080/Blog/services";
    }
    private final Integer id=1;
    private final Integer invalidId=-1;
    private final Integer idNotFound=1000;
    private final String invalidText="a";
    private final String invalidCategory="invalid";
    private final String invalidUsername="invalid";


    @Test
    public void Should_Return_Post_post1_When_Get_Post_By_Id_And_Id_1(){
        String jsonString=when().get("/post/{id}",id).asString();
        String text= JsonPath.from(jsonString).get("text");

        Assert.assertEquals("post1",text);
    }

    @Test
    public void Should_Return_Error_Message_When_Get_Post_By_Id_And_Id_Invalid(){
        when().get("/post/-1").then().body(Matchers.equalTo("Invalid id"));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Get_Post_By_Id_And_Post_Not_Found(){
        when().get("/post/{id}",idNotFound).then().body(Matchers.equalTo("Post not found"));
    }

    @Test
    public void Should_Return_All_Posts_When_Get_All_Posts(){
        String jsonString=when().get("/post").asString();
        List<String> texts= JsonPath.from(jsonString).get("text");

        Assert.assertEquals("post1",texts.get(0));
        Assert.assertEquals("post2",texts.get(1));
        Assert.assertEquals("post3",texts.get(2));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Add_Post_And_Text_Less_Than_3_Characters(){
        Map<String,String> map=new HashMap<>();
        map.put("text",invalidText);
        map.put("username","user1");
        map.put("category","Java");

        given().contentType("application/json")
                .body(map)
                .when()
                .post("/post").then()
                .statusCode(500)
                .and()
                .body(Matchers.equalTo("Post text must be between 3 and 10 characters"));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Add_Post_And_Category_Not_Found(){
        Map<String,String> map=new HashMap<>();
        map.put("text","post");
        map.put("username","user1");
        map.put("category",invalidCategory);

        given().contentType("application/json")
                .body(map)
                .when()
                .post("/post").then()
                .statusCode(404)
                .and()
                .body(Matchers.equalTo("Category not found"));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Add_Post_And_User_Not_Found(){
        Map<String,String> map=new HashMap<>();
        map.put("text","post");
        map.put("username",invalidUsername);
        map.put("category","category1");

        given().contentType("application/json")
                .body(map)
                .when()
                .post("/post").then()
                .statusCode(404)
                .and()
                .body(Matchers.equalTo("User not found"));
    }

    @Test
    public void Should_Return_200_When_Update_Post(){
        Map<String,String> map=new HashMap<>();
        map.put("text","post");
        map.put("username","user1");
        map.put("category","category1");

        given().contentType("application/json")
                .body(map)
                .when()
                .put("/post/{id}",id)
                .then()
                .statusCode(200)
                .and()
                .body(Matchers.equalTo("Post updated"));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Update_Post_And_Id_Invalid(){
        Map<String,String> map=new HashMap<>();
        map.put("text","post");
        map.put("username","user1");
        map.put("category","Java");

        given().contentType("application/json")
                .body(map)
                .when()
                .put("/post/{id}",invalidId)
                .then()
                .statusCode(500)
                .and()
                .body(Matchers.equalTo("Invalid id"));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Update_Post_And_Post_Not_Found(){
        Map<String,String> map=new HashMap<>();
        map.put("text","post");
        map.put("username","user1");
        map.put("category","category1");

        given().contentType("application/json")
                .body(map)
                .when()
                .put("/post/{id}",idNotFound)
                .then()
                .body(Matchers.equalTo("Post not found"));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Update_Post_And_User_Not_Found(){
        Map<String,String> map=new HashMap<>();
        map.put("text","post");
        map.put("username",invalidUsername);
        map.put("category","category1");

        given().contentType("application/json")
                .body(map)
                .when()
                .put("/post/{id}",id)
                .then()
                .body(Matchers.equalTo("User not found"));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Update_Post_And_Category_Not_Found(){
        Map<String,String> map=new HashMap<>();
        map.put("text","post");
        map.put("username","user1");
        map.put("category",invalidCategory);

        given().contentType("application/json")
                .body(map)
                .when()
                .put("/post/{id}",id)
                .then()
                .body(Matchers.equalTo("Category not found"));
    }

    @Test
    public void Should_Return_Post_post1_When_Get_Posts_By_Filter_And_Start_0_Length_1(){
        String json=given()
                .queryParam("start","0")
                .queryParam("length","1")
                .when().get("/post").asString();

        List<String> texts=JsonPath.from(json).get("text");
        Assert.assertEquals("post1",texts.get(0));
    }

    @Test
    public void Should_Return_Post_post2_And_post3_When_Get_Posts_By_Filter_And_Start_1_Length_2(){
        String json=given()
                .queryParam("start","1")
                .queryParam("length","2")
                .when().get("/post").asString();

        List<String> texts=JsonPath.from(json).get("text");
        Assert.assertEquals("post2",texts.get(0));
        Assert.assertEquals("post3",texts.get(1));
    }

    @Test
    public void Should_Return_All_Posts_When_Get_Posts_By_Filter_And_Start_0_Length_0(){
        String json=given()
                .queryParam("start","0")
                .queryParam("length","0")
                .when().get("/post").asString();

        List<String> texts=JsonPath.from(json).get("text");
        Assert.assertEquals("post1",texts.get(0));
        Assert.assertEquals("post2",texts.get(1));
        Assert.assertEquals("post3",texts.get(2));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Get_Posts_By_Filter_And_Start_Negative(){
        given()
                .queryParam("start","-1")
                .queryParam("length","0")
                .when()
                .get("/post").then()
                .body(Matchers.equalTo("Start parameter cannot be negative"))
                .and()
                .statusCode(500);
    }

    @Test
    public void Should_Return_ErrorMessage_When_Get_Posts_By_Filter_And_Length_Negative(){
        given()
                .queryParam("start","0")
                .queryParam("length","-1")
                .when()
                .get("/post").then()
                .body(Matchers.equalTo("Length parameter cannot be negative"))
                .and()
                .statusCode(500);
    }

    @Test
    public void Should_Return_Post_post2_And_post3_When_Get_Posts_By_Filter_And_Start_1_Length_1000(){
        String json=given()
                .queryParam("start","1")
                .queryParam("length","1000")
                .when().get("/post").asString();

        List<String> texts=JsonPath.from(json).get("text");
        Assert.assertEquals("post2",texts.get(0));
        Assert.assertEquals("post3",texts.get(1));
    }

    @Test
    public void Should_Return_Category_Java_When_Get_Category_Of_Post_And_Id_1(){
        String json=given()
                .when().get("/post/{id}/category",id).asString();

        String name = JsonPath.from(json).get("name");
        Assert.assertEquals("category1",name);
    }
    @Test
    public void Should_Return_ErrorMessage_When_Get_Category_Of_Post_And_Id_Negative(){
        when().get("/post/{id}/category",invalidId)
                .then()
                .body(Matchers.equalTo("Invalid id"))
                .and()
                .statusCode(500);
    }

    @Test
    public void Should_Return_ErrorMessage_When_Get_Category_Of_Post_And_Post_Not_Found(){
        when().get("/post/{id}/category",idNotFound)
                .then()
                .body(Matchers.equalTo("Post not found"))
                .and()
                .statusCode(404);
    }


    @Test
    public void Should_Return_ErrorMessage_When_Get_User_Of_Post_And_Id_Negative(){
        when().get("/post/{id}/user",invalidId)
                .then()
                .body(Matchers.equalTo("Invalid id"))
                .and()
                .statusCode(500);
    }

    @Test
    public void Should_Return_ErrorMessage_When_Get_User_Of_Post_And_Post_Not_Found(){
        when().get("/post/{id}/user",idNotFound)
                .then()
                .body(Matchers.equalTo("Post not found"))
                .and()
                .statusCode(404);
    }

    @Test
    public void Should_Return_User_user1_When_Get_User_Of_Post_And_Id_1(){
        String json=given()
                .when().get("/post/1/user").asString();

        String name = JsonPath.from(json).get("userinfo.name");
        Assert.assertEquals("user1",name);
    }

    @Test
    public void Should_Return_User_user3_When_Get_User_Of_Post_And_Id_3(){
        String json=given()
                .when().get("/post/3/user").asString();

        String name = JsonPath.from(json).get("userinfo.name");
        Assert.assertEquals("user3",name);
    }








}
