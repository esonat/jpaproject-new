package com.example.rest.resource;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlMetadataHandler;
import org.dbunit.operation.DatabaseOperation;
import org.hamcrest.Matchers;
import org.junit.*;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.*;

import static io.restassured.RestAssured.*;

public class UserResourceTest{


    private final String jdbcURL="jdbc:mysql://localhost:3306/JPAProject?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final String driver="com.mysql.cj.jdbc.Driver";
    private final String username="root";
    private final String password="sonat";

    private final int USER_COUNT=3;

    @BeforeClass
    public static void createSchema() throws Exception{

    }

    private IDataSet readDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new File("full.xml"));
    }

    private void cleanlyInsertDataset(IDataSet dataSet) throws Exception{

        IDatabaseTester databaseTester=new JdbcDatabaseTester(driver,jdbcURL,username,password);
        databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
    }


    @Before
    public void setup() throws Exception{
        Class.forName (driver).newInstance ();
        Connection conn = DriverManager.getConnection (jdbcURL, "root", "sonat");

        IDatabaseConnection dbUnitConn = new DatabaseConnection(conn,"JPAProject");
        DatabaseConfig dbCfg = dbUnitConn.getConfig();
        dbCfg.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);
        dbCfg.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
        dbCfg.setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());

        IDataSet dataSet = new FlatXmlDataSetBuilder().build(new File("full.xml"));
        DatabaseOperation.CLEAN_INSERT.execute(dbUnitConn,dataSet);

        RestAssured.baseURI="http://localhost:8080/Blog/services";
    }
    private final Integer id=1;
    private final Integer invalidId=-1;
    private final Integer idNotFound=1000;
    private String[] validNames=new String[]{"testUser1","testUser2","testUser3","testUser4","testUser5"};
    private int counter=0;

    public UserResourceTest(){
    }

    @Test
    public void Should_Return_1_When_Id_1(){
        get("/user/1").then().assertThat().body("id", Matchers.is(1));
    }

    @Test
    public void Should_Return_2_When_Id_2(){
        get("/user/2").then().assertThat().body("id",Matchers.is(2));
    }
    @Test
    public void Should_Return_500_When_InvalidId(){
        given().when().get("/user/{id}",invalidId).then().statusCode(500);
    }

    @Test
    public void Should_Return_404_When_User_Not_Found() {
        given().when().get("/user/1000").then().statusCode(404);
    }

    @Test
    public void When_Id_1_Then_Return_1_User(){
        Response response=when().get("/user/1")
                .then().contentType(ContentType.JSON)
                .extract().response();

        String jsonAsString=response.asString();

        ArrayList<Map<String,String>> jsonAsArrayList=new ArrayList<Map<String,String>>();
        jsonAsArrayList.add(JsonPath.from(jsonAsString).get(""));

        Assert.assertEquals(1,jsonAsArrayList.size());
    }

    @Test
    public void When_Id_2_Then_Return_1_User(){
        Response response=when().get("/user/2")
                .then().contentType(ContentType.JSON)
                .extract().response();

        String jsonAsString=response.asString();

        ArrayList<Map<String,String>> jsonAsArrayList=new ArrayList<Map<String,String>>();
        jsonAsArrayList.add(JsonPath.from(jsonAsString).get(""));

        Assert.assertEquals(1,jsonAsArrayList.size());
    }

    @Test
    public void Should_Return_200_When_Get_All_Users(){
        given().when().get("/user").then().statusCode(200);
    }


    @Test
    public void When_Post_User_Then_Return_200(){
        String validName=validNames[counter++];

        Map<String,String> map=new HashMap<>();
        map.put("name","testUser1");
        map.put("city","Istanbul");
        map.put("country","Turkey");
        map.put("street","acibadem");
        map.put("state","Istanbul");
        map.put("zip","34662");
        map.put("username","testUser1");
        map.put("password","1234");
        map.put("phone","1234");

        given().contentType("application/json")
                .body(map)
                .when()
                .post("/user").then()
                .statusCode(200);
    }

    @Test
    public void Should_Return_Users_When_Get_All_Users(){

        String jsonAsString=get("/user").asString();
        List<String> usernames=JsonPath.from(jsonAsString).get("userLoginInfo.username");
        List<String> passwords=JsonPath.from(jsonAsString).get("userLoginInfo.password");
        List<String> names=JsonPath.from(jsonAsString).get("userinfo.name");

        Assert.assertEquals(3,usernames.size());
        Assert.assertEquals("user1",usernames.get(0));
        Assert.assertEquals("user2",usernames.get(1));
        Assert.assertEquals("user1",names.get(0));
        Assert.assertEquals("user2",names.get(1));
    }

    @Test
    public void Should_Return_1_User_When_Query_Limit_Is_1(){
        String jsonAsString=given().queryParam("limit","1")
                .when().get("/user").asString();

        List<String> names=JsonPath.from(jsonAsString).get("userinfo.name");

        Assert.assertEquals(1,names.size());
    }

    @Test
    public void Should_Return_2_Users_When_Query_Limit_Is_2(){
        String jsonAsString=given().queryParam("limit","2")
                .when().get("/user").asString();

        List<String> names=JsonPath.from(jsonAsString).get("userinfo.name");

        Assert.assertEquals(2,names.size());
    }

    @Test
    public void Should_Return_User_user2_When_Query_Range_From_1_To_1(){
        String jsonAsString=given()
                .queryParam("start","1")
                .queryParam("limit","1")
                .when().get("/user").asString();

        List<String> names=JsonPath.from(jsonAsString).get("userinfo.name");
        List<String> usernames = JsonPath.from(jsonAsString).get("userLoginInfo.username");
        List<String> passwords= JsonPath.from(jsonAsString).get("userLoginInfo.password");

        Assert.assertEquals(1,names.size());
        Assert.assertEquals("user2",names.get(0));
        Assert.assertEquals("user2",usernames.get(0));
        Assert.assertEquals("user2",passwords.get(0));
    }

    @Test
    public void Should_Return_User_user1_And_user2_When_Query_Range_From_0_to_1(){
        String jsonAsString=given()
                .queryParam("start","0")
                .queryParam("limit","2")
                .when().get("/user").asString();

        List<String> names=JsonPath.from(jsonAsString).get("userinfo.name");
        List<String> usernames = JsonPath.from(jsonAsString).get("userLoginInfo.username");
        List<String> passwords= JsonPath.from(jsonAsString).get("userLoginInfo.password");

        Assert.assertEquals(2,names.size());
        Assert.assertEquals("user1",names.get(0));
        Assert.assertEquals("user1",usernames.get(0));
        Assert.assertEquals("user1",passwords.get(0));
        Assert.assertEquals("user2",names.get(1));
        Assert.assertEquals("user2",usernames.get(1));
        Assert.assertEquals("user2",passwords.get(1));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Query_Limit_Less_Than_0(){
        given().queryParam("limit","-1").when().get("/user").then().body(Matchers.equalTo("Query limit cannot be less than 0"))
                .and()
                .statusCode(500);
    }

    @Test
    public void Should_Return_ErrorMessage_When_Start_GreaterThan_Size(){
        given().queryParam("start","5").when().get("/user").then().body(Matchers.equalTo("Invalid start parameter"))
                .and()
                .statusCode(500);
    }

    @Test
    public void Should_Return_All_Users_When_Query_Start_0_And_Limit_0(){
        String jsonAsString= given()
                .queryParam("start","0")
                .queryParam("limit","0")
                .when().get("/user").asString();

        List<String> names=JsonPath.from(jsonAsString).get("userinfo.name");

        Assert.assertEquals(3,names.size());
        Assert.assertEquals("user1",names.get(0));
        Assert.assertEquals("user2",names.get(1));
        Assert.assertEquals("user3",names.get(2));
    }

    @Test
    public void Should_Return_All_Users_When_Start_Null_And_Limit_Null(){
        String jsonAsString= given()
                .when().get("/user").asString();

        List<String> names=JsonPath.from(jsonAsString).get("userinfo.name");

        Assert.assertEquals(3,names.size());
        Assert.assertEquals("user1",names.get(0));
        Assert.assertEquals("user2",names.get(1));
        Assert.assertEquals("user3",names.get(2));
    }

    @Test
    public void Should_Return_Null_When_Start_Negative_And_Limit_Null(){
        String json=given()
                    .queryParam("start","-1")
                    .when().get("/user").asString();

        List<String> name=JsonPath.from(json).get("name");
        Assert.assertEquals(0,name.size());
    }

    @Test
    public void Should_Return_user2_And_user3_When_Start_1_Limit_3(){
        String jsonString=given()
                .queryParam("start","1")
                .queryParam("limit","3")
                .when()
                .get("/user").asString();

        List<String> names=JsonPath.from(jsonString).get("userinfo.name");

        Assert.assertEquals(2,names.size());
        Assert.assertEquals("user2",names.get(0));
        Assert.assertEquals("user3",names.get(1));
    }

    @Test
    public void Should_Return_Error_Message_When_Post_User_And_Password_6_Characters(){

        Map<String,String> map=new HashMap<>();
        map.put("name","testUser1");
        map.put("city","Istanbul");
        map.put("country","Turkey");
        map.put("street","acibadem");
        map.put("state","Istanbul");
        map.put("zip","34662");
        map.put("username","testUser1");
        map.put("password","123456");
        map.put("phone","1234567");

        given().contentType("application/json")
                .body(map)
                .when()
                .post("/user").then()
                .body(Matchers.equalTo("Password must be between 3 and 5 characters"))
                .and()
                .statusCode(500);
    }

    @Test
    public void Should_Return_ErrorMessage_When_Post_User_And_Invalid_City(){
        Map<String,String> map=new HashMap<>();
        map.put("name","testUser1");
        map.put("city","Ankara");
        map.put("country","Turkey");
        map.put("street","acibadem");
        map.put("state","Ankara");
        map.put("zip","34662");
        map.put("username","testUser1");
        map.put("password","1234");
        map.put("phone","1234567");

        given().contentType("application/json")
                .body(map)
                .when()
                .post("/user").then()
                .body(Matchers.equalTo("City not found"))
                .and()
                .statusCode(500);
    }

    @Test
    public void Should_Return_ErrorMessage_When_Post_User_And_Duplicate_Username(){
        Map<String,String> map=new HashMap<>();
        map.put("name","testUser1");
        map.put("city","Istanbul");
        map.put("country","Turkey");
        map.put("street","acibadem");
        map.put("state","Ankara");
        map.put("zip","34662");
        map.put("username","user1");
        map.put("password","1234");
        map.put("phone","1234567");

        given().contentType("application/json")
                .body(map)
                .when()
                .post("/user").then()
                .body(Matchers.equalTo("Duplicate username"))
                .and()
                .statusCode(500);
    }

    @Test
    public void Should_Return_ErrorMessage_When_Post_User_And_Duplicate_Phone(){
        Map<String,String> map=new HashMap<>();
        map.put("name","testUser1");
        map.put("city","Istanbul");
        map.put("country","Turkey");
        map.put("street","acibadem");
        map.put("state","Istanbul");
        map.put("zip","34662");
        map.put("username","testUser1");
        map.put("password","1234");
        map.put("phone","123");

        given().contentType("application/json")
                .body(map)
                .post("/user")
                .then()
                .body(Matchers.equalTo("Duplicate phone"))
                .and()
                .statusCode(500);
    }







}