package com.example.rest.resource;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlMetadataHandler;
import org.dbunit.operation.DatabaseOperation;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.junit.Assert.assertEquals;

/**
 * Created by sonat on 17.09.2016.
 */
public class CategoryResourceTest {
    private final String jdbcURL="jdbc:mysql://localhost:3306/JPAProject?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final String mysql_driver="com.mysql.cj.jdbc.Driver";
    private final String mysql_username="root";
    private final String mysql_password="sonat";

    private final int POST_COUNT=3;

    @BeforeClass
    public static void createSchema() throws Exception{

    }

    private IDataSet readDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new File("full.xml"));
    }

    private void cleanlyInsertDataset(IDataSet dataSet) throws Exception{

        IDatabaseTester databaseTester=new JdbcDatabaseTester(mysql_driver,jdbcURL,mysql_username,mysql_password);
        databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
    }


    @Before
    public void setup() throws Exception{
        Class.forName (mysql_driver).newInstance ();
        Connection conn = DriverManager.getConnection (jdbcURL, "root", "sonat");

        IDatabaseConnection dbUnitConn = new DatabaseConnection(conn,"JPAProject");
        DatabaseConfig dbCfg = dbUnitConn.getConfig();
        dbCfg.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);
        dbCfg.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
        dbCfg.setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());

        IDataSet dataSet = new FlatXmlDataSetBuilder().build(new File("full.xml"));
        DatabaseOperation.CLEAN_INSERT.execute(dbUnitConn,dataSet);

        RestAssured.baseURI="http://localhost:8080/Blog/services";
    }

    private String[] validCategoryNames=new String[]{"category10","category11",
    "category12","category13","category14","category15","category16"};
    private int counter=0;

    private final Integer id=1;
    private final Integer invalidId=-1;
    private final Integer idNotFound=1000;
    private final String invalidText="a";
    private final String invalidCategory="invalid";
    private final String invalidUsername="invalid";
    private final String validCategoryName="category10";
    private final String smallCategoryName="ab";
    private final String longCategoryName="categoryNameTooLong";
    private final String invalidCategoryName="category!*";
    
    @Test
    public void Should_Return_category1_When_Get_Category_By_Id_And_Id_1(){
        String json=given().when().get("/category/{id}",id).asString();


        String name= JsonPath.from(json).get("name");
        assertEquals("category1",name);
    }

    @Test
    public void Should_Return_ErrorMessage_When_Get_Category_By_Id_And_Id_Invalid(){
        when().get("/category/{id}",invalidId)
                .then()
                .body(Matchers.equalTo("Id cannot be less than 1"))
                .and().statusCode(500);
    }

    @Test
    public void Should_Return_NotFound_When_Get_Category_By_Id_And_Category_Not_Found(){
        when().get("/category/{id}",idNotFound)
                .then()
                .body(Matchers.equalTo("Category not found"))
                .and().statusCode(404);
    }

    @Test
    public void Should_Return_Category_category2_When_Get_Category_By_Name_And_Name_category2(){
        String json=given().queryParam("name","category2")
                .when()
                .get("/category").asString();

        String name= JsonPath.from(json).get("name");
        assertEquals("category2",name);
    }

    @Test
    public void Should_Return_NotFound_When_Get_Category_By_Name_And_Name_Invalid(){
        given().queryParam("name",invalidCategory)
                .when().get("/category")
                .then()
                .body(Matchers.equalTo("Category not found"))
                .and().statusCode(404);
    }

    @Test
    public void Should_Return_ErrorMessage_When_Add_Category_And_Category_Name_Less_Than_3_Characters(){
        Map<String,String> map=new HashMap<>();
        map.put("name",smallCategoryName);

        given().contentType("application/json")
                .body(map)
                .when()
                .post("/category").then()
                .statusCode(500)
                .and()
                .body(Matchers.equalTo("Category name must be between 3 and 15 characters"));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Add_Category_And_Category_Name_More_Than_15_Characters(){
        Map<String,String> map=new HashMap<>();
        map.put("name",longCategoryName);

        given().contentType("application/json")
                .body(map)
                .when()
                .post("/category").then()
                .statusCode(500)
                .and()
                .body(Matchers.containsString("Category name must be between 3 and 15 characters"));
    }

    @Test
    public void Should_Return_ErrorMessage_When_Add_Category_And_Category_Name_Invalid(){
        Map<String,String> map=new HashMap<>();
        map.put("name",invalidCategoryName);

        given().contentType("application/json")
                .body(map)
                .when()
                .post("/category").then()
                .statusCode(500)
                .and()
                .body(Matchers.containsString("Invalid category name"));
    }

    @Test
    public void Should_Return_204_When_Update_Category(){
        Map<String,String> map=new HashMap<>();
        map.put("name",validCategoryNames[counter++]);

        given().contentType("application/json")
                .body(map)
                .when()
                .put("/category/{id}",id).then()
                .statusCode(204);
    }


    @Test
    public void Should_Return_ErrorMessage_When_Update_Category_And_Id_Invalid(){
        Map<String,String> map=new HashMap<>();
        map.put("name",validCategoryNames[counter++]);

        given().contentType("application/json")
                .body(map)
                .when()
                .put("/category/{id}",invalidId).then()
                .statusCode(500)
                .and()
                .body(Matchers.containsString("Invalid id"));
    }



    @Test
    public void Should_Return_ErrorMessage_When_Update_Category_And_Names_Are_Equal(){
        Map<String,String> map=new HashMap<>();
        map.put("name","category3");

        given().contentType("application/json")
                .body(map)
                .when()
                .put("/category/3").then()
                .statusCode(500)
                .and()
                .body(Matchers.containsString("Category name must be unique"));
    }

    @Test
    public void Should_Return_NotFound_When_Update_Category_And_Category_Not_Found(){
        Map<String,String> map=new HashMap<>();
        map.put("name",validCategoryNames[counter++]);

        given().contentType("application/json")
                .body(map)
                .when()
                .put("/category/{id}",idNotFound).then()
                .statusCode(404)
                .and()
                .body(Matchers.containsString("Category not found"));
    }


}
