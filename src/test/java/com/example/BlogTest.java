package com.example;

import com.example.connection.ClientManager;
import com.example.connection.ClientManagerRemote;
import com.example.entities.*;
import com.example.service.BlogService;
import com.example.task.TaskManager;
import com.example.util.SQLUtil;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;
import javax.naming.NamingException;
import java.io.*;
import java.util.*;


@RunWith(Arquillian.class)
public class BlogTest {

    @Deployment
    public static Archive<?> createDeployment() {
        //JavaArchive jar= ShrinkWrap.create(JavaArchive.class,"test.jar");

        JavaArchive jar = ShrinkWrap.create(
                JavaArchive.class, "test.jar")
                .addClass(Client.class)
                .addClass(BlogService.class)
                .addClass(ClientManager.class)
                .addClass(ClientManagerRemote.class)
                //.addClass(ApplicationManager.class)
                .addClass(TaskManager.class)
                .addClass(Task.class)
                .addClass(User.class)
                .addClass(City.class)
                .addClass(Country.class)
                .addClass(Role.class)
                .addClass(UserRole.class)
                .addClass(Comment.class)
                .addClass(Post.class)
                .addClass(Document.class)
                .addClass(Category.class)
                .addPackage(Phone.class.getPackage())
                .addAsManifestResource("test-persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

        //   return jar;
        return jar;
    }


    @EJB
    BlogService blogService;

    @EJB
    ClientManagerRemote clientManager;

//    @EJB
//    ClientManager clientManager;

    @BeforeClass
    public static void init() throws NamingException {
//        InitialContext ctx=new InitialContext();
//        clientManager=(ClientManagerRemote)ctx.lookup("java:global/Blog/ClientManager!com.example.connection.ClientManagerRemote");

    }


    @Test
    public void testBlogRemoveClients() throws Exception {

//        User user=blogService.getUserByUsername("engin");
//        RestClient client=new RestClient("client-1",9090,true,user,"POST",new Date(),new Date());

        blogService.removeClient("client-1");
    }

    @Test
    public void testRemoveClients() {
        try {

//            InitialContext ctx=new InitialContext();
//            ClientManagerRemote clientManager=(ClientManagerRemote)ctx.lookup("java:global/test.jar/test/ClientManagerRemote");


            clientManager.addClient("client-1", "8080", "engin", ClientRole.POST.toString());
            clientManager.addClient("client-2", "9090", "engin", ClientRole.CATEGORY.toString());
            clientManager.addClient("client-3", "9010", "engin", ClientRole.CATEGORY.toString());

            System.out.println(clientManager.getConnectedClients().size());

            PrintConnectedClients();

            List<String> removed = new ArrayList<String>();
            removed.add("client-1");
            removed.add("client-2");

            clientManager.removeClients(removed);
            PrintConnectedClients();

//            Assert.assertEquals(clientManager.getConnectedClients().size(), 1);
//            Assert.assertEquals(clientManager.getConnectedClients().get(0).getId(), "client-3");

            System.out.println(clientManager.getConnectedClients().get(0).getId());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


//    @Inject
//    ApplicationManager applicationManager;

//    @Inject
//    TaskManager taskManager;

//    @Ignore
//    public void testRemoveClients(){
////        try {
////            clientManager.addClient("client-1", "9090", "engin", "POST");
////        }catch (Exception e){
////            System.out.println(e.getMessage());
////        }
//    }


    @Ignore
    public void testAddClient() {

    }

    //    @Ignore
//    public void testApplicationManager(){
//        Assert.assertNotNull(applicationManager);
//
//    }
    @Ignore
    public void testProperties() {
        // PropertiesUtil.writeProperties();
    }

    @Ignore
    public void writeToMongo() {

//        String deneme="application.started.false";
//        String[] arr=StringUtils.split(deneme,'.');
//
//        Map<String,Object> map=new HashMap<String,Object>();
//        map.put("application.started","false");
//
//
//        org.bson.Document searchQuery = new org.bson.Document().append("application", "started");
//
//        collection.update(searchQuery, newDocument);
//
//        MongoUtil.write("config",map);
//        char[] arr=new char[3];
//        arr[0]='a';
//        arr[1]='b';
//        arr[2]='c';
//        int len=arr.length;
//        System.out.println(arr.length);
//
//        for(int index=len-1;index>=0;index--){
//            System.out.println(arr[index]);
//        }
//
//        Map<String,Object> map=new HashMap<String,Object>();
//        map.put("application.started","true");
//        MongoUtil.write("config",map);
//        try{
//            MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
//            MongoDatabase DB = mongoClient.getDatabase("JPAProjectConfigDB");
//
//
//            Map<Object,Object> map = new HashMap<Object,Object>();
//            map.put("1", "Department A");
//            map.put("2", "Department B");
//            DB.getCollection("config")
//                    .insertOne(new org.bson.Document()
//                    .append("application",new org.bson.Document("started","true")));
//
//        }catch(Exception e){
//            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
//        }
    }

    @Ignore
    public void testPropertiesFile() throws IOException {
//        Properties props = PropertiesUtil.getProperties();
//
//        String category=props.getProperty("startup.tasks.category");
//        String[] list= props.getProperty("startup.tasks").split(",");
//
//        for(int i=0;i<list.length;i++){
//            System.out.println(list[i]);
//        }
//
//        System.out.println(category);
    }

    @Ignore
    public void testExeucuteSQL() {
        try {

//            String myDriver = "com.mysql.cj.jdbc.Driver";
//            String myUrl = "jdbc:mysql://localhost/JPAProject?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
//            Class.forName(myDriver);
//            Connection conn = DriverManager.getConnection(myUrl, "root", "sonat");

            SQLUtil.executeStartupScript();

            //ConnectionString connectionString = new ConnectionString("localhost:3306/JPAProject", null);
            //SQLUtil.executeSqlScript(conn,new File("/home/sonat/Documents/JPAProjectConfig/JPAProjectStartup.sql"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Ignore
    public void testWriteProperties() {

        try {
            File configFile = new File("/home/sonat/Documents/JPAProjectConfig/application.properties");

            Properties props = new Properties();
            props.setProperty("application.started", "true");
            FileWriter writer = new FileWriter(configFile);
            props.store(writer, null);
            writer.close();

        } catch (FileNotFoundException ex) {
            System.out.println("WRITE PROPERTIES:" + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("WRITE PROPERTIES:" + ex.getMessage());
        }
    }

    @Ignore
    public void testReadProperties() {
        try {
            Properties props = new Properties();
            // URL url = new URL("/home/sonat/Documents/JPAProject/application.properties");
            File file = new File("/home/sonat/Documents/JPAProjectConfig/application.properties");

            Reader reader = new FileReader(file);
            props.load(reader);

            System.out.println(props.getProperty("application.started"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
//        File configFile = new File("src/main/resources/application.properties");
//
//        try {
//            Properties props = new Properties();
//            props.setProperty("host", "www.codejava.net");
//            FileWriter writer = new FileWriter(configFile);
//            props.store(writer, "host settings");
//            writer.close();
//        } catch (FileNotFoundException ex) {
//            // file does not exist
//        } catch (IOException ex) {
//            // I/O error
//        }
    }

    @Ignore
    public void testTaskManager() {
//        try {
//            Properties props = PropertiesUtil.getProperties();
//
//            String category = props.getProperty("startup.tasks.category");
//            String[] list = props.getProperty("startup.tasks").split(",");
//
//            //Assert.assertEquals(2,list.length);
//            //Assert.assertEquals(0,taskManager.getTasks().size());
//
//            for (int index = 0; index < list.length; index++) {
//
//                String taskType = list[index];
//
//                Task task = new Task();
//                task.setCategory(category);
//                task.setType(taskType);
//                task.setLongRunning(true);
//                task.setClients(null);
//
//                taskManager.addTask(task);
//            }
//
//            taskManager.removeTask(0);
//            //Assert.assertEquals(3,taskManager.getTasks().size());
//
//
//            Assert.assertEquals(2,taskManager.getTasks().size());
//
//            for (Task task : taskManager.getTasks()) {
//                System.out.println(task.toString());
//            }
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }
    }

    @Ignore
    public void testBlogService() throws IOException {
//        Properties props = PropertiesUtil.getProperties("application.properties");
//
//        String category=props.getProperty("startup.tasks.category");
//        String[] list= props.getProperty("startup.tasks").split(",");
//
//        for(int index=0;index<list.length;index++){
//
//            String taskType=list[index];
//
//            Task task=new Task();
//            task.setCategory(category);
//            task.setType(taskType);
//            task.setLongRunning(true);
//            task.setClients(null);
//
//            taskManager.addTask(task);
//        }
    }

    @Ignore
    public void testAddClient_ClientManager() {
//        RestClient client=new RestClient();
//        client.setStatus(true);
//        client.setPort(9090);
//        client.setClientRole(ClientRole.POST.toString());
//        client.setLatestActivity(new Date());
//        client.setLatestMessage(new Date());
//        client.setId("client-1");


    }

    public List<Client> list() {
        List<Client> clients = new ArrayList<Client>();

        Client client = new Client();
        client.setStatus(true);
        client.setPort(8080);
        client.setClientRole(ClientRole.POST.toString());

        Calendar calendar = Calendar.getInstance();
        calendar.set(9, 0, 0);

        client.setLatestActivity(calendar.getTime());
        client.setLatestMessage(calendar.getTime());
        client.setId("client-1");

        Client client2 = new Client();
        client2.setStatus(true);
        client2.setPort(9090);
        client2.setClientRole(ClientRole.POST.toString());
        client2.setLatestActivity(new Date());
        client2.setLatestMessage(new Date());
        client2.setId("client-2");

        //clientManager.addClient("client-3","9010","engin",ClientRole.COMMENT.toString());


        clients.add(client);
        clients.add(client2);
        return clients;
    }

    public void PrintConnectedClients() {
//        for(RestClient client:clientManager.getConnectedClients()){
//            System.out.println(client.toString());
//        }
    }

    public void PrintRole(ClientRole role) {
        if (role.getClass().equals(ClientRole.class))
            System.out.println(role.toString());
    }

    @Ignore
    public void testRetainAll() {
        Queue<String> list1 = new LinkedList<String>();
        Queue<String> list2 = new LinkedList<String>();

        list1.add("a");
        list1.add("b");
        list1.add("c");

        list2.add("a");
        list2.add("d");
        list2.add("f");


        for (String item : list2) {
            if (!list1.contains(item)) System.out.println(item);
        }

    }


    @Ignore
    public void testCompareTasks() {
        Task item1 = new Task();
        item1.setLongRunning(true);
        item1.setType(TaskType.TASK_REMOVEDISCONNECTED.toString());
        item1.setStartTime(new Date());
        item1.setLatestActivity(new Date());
        item1.setCategory(TaskCategory.CLIENTTASK.toString());
        item1.setCompleted(false);
        item1.setClients(null);

        Task item2 = new Task();
        item2.setLongRunning(true);
        item2.setType(TaskType.TASK_REMOVEDISCONNECTED.toString());
        item2.setStartTime(new Date());
        item2.setLatestActivity(new Date());
        item2.setCategory(TaskCategory.CLIENTTASK.toString());
        item2.setCompleted(false);
        item2.setClients(null);

        item1.setCategory(TaskCategory.MESSAGETASK.toString());
        item1.setType(TaskType.TASK_SENDCATEGORYLISTMESSAGE.toString());

        item2.setCategory(TaskCategory.MESSAGETASK.toString());
        item2.setType(TaskType.TASK_SENDCATEGORYLISTMESSAGE.toString());

        Client client1 = new Client();
        client1.setLatestMessage(new Date());
        client1.setId("client-1");

        Client client2 = new Client();
        client2.setLatestMessage(new Date());
        client2.setId("client-2");

        Client client3 = new Client();
        client3.setLatestMessage(new Date());
        client3.setId("client-3");

        item1.setClients(new ArrayList<Client>());
        item1.getClients().add(client1);
        //item1.getClients().add(client2);

        item2.setClients(new ArrayList<Client>());
        item2.getClients().add(client2);
        item2.getClients().add(client3);
        //item2.getClients().add(client3);
        item1.setCompleted(false);

        Assert.assertEquals(true, item1.noConflict(item2));
//
//        boolean contains=true;
//
//        for(RestClient client:item2.getClients()){
//            if(!item1.getClients().contains(client)) {
//                System.out.println("CONTAINS FALSE");
//                contains=false;
//            }
//        }

        //System.out.println(item1.getCategory().equals(TaskCategory.MESSAGETASK.toString()));
        //     Assert.assertTrue(item1.taskConflict(item2));
    }

    @Ignore
    public void testClientRole() {
        ClientRole role = ClientRole.CATEGORY;

        PrintRole(role);
    }


    @Ignore
    public void testUpdateTask() throws Exception {
//        clientManager.addClient("client-1","8080","engin",ClientRole.POST.toString());
//        clientManager.addClient("client-2","9090","engin",ClientRole.CATEGORY.toString());
//        clientManager.addClient("client-3","9010","engin",ClientRole.CATEGORY.toString());
//
//        //taskList.Task_RemoveDisconnectedClients();
//
//        Assert.assertEquals(clientManager.getConnectedClients().size(),3);
//
//        Task task=blogService.getTaskByType(TaskType.TASK_REMOVEDISCONNECTED.toString());
//        Assert.assertNotNull(task);
//        Assert.assertEquals(task.getType(),TaskType.TASK_REMOVEDISCONNECTED.toString());
//
//
//     //   taskManager.updateTask(TaskType.TASK_REMOVEDISCONNECTED.toString());
//        task=blogService.getTaskByType(TaskType.TASK_REMOVEDISCONNECTED.toString());
//
//        Date time=task.getLatestActivity();
//        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
//        calendar.setTime(time);   // assigns calendar to given date
//        int minute=calendar.get(Calendar.MINUTE); // gets hour in 24h format
//
//        Date now=new Date();
//        calendar.setTime(now);
//        int minuteNow=calendar.get(Calendar.MINUTE);
//
//        Assert.assertEquals(minute,minuteNow);
    }

    @Ignore
    public void testAssignClientRole() {
//         try{
//            clientManager.addClient("client-1","8080","engin",ClientRole.POST.toString());
//            clientManager.addClient("client-2","9090","engin",ClientRole.CATEGORY.toString());
//
//            PrintConnectedClients();
//            //taskList.Task_AssignClientRole();
//            PrintConnectedClients();
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }
    }

    @Ignore
    public void testRemoveDisconnectedClients() {
//       try {
//           clientManager.addClient("client-1", "8080", "engin", ClientRole.POST.toString());
//           clientManager.addClient("client-2", "9090", "engin", ClientRole.CATEGORY.toString());
//
//           Calendar calendar = Calendar.getInstance();
//           calendar.set(Calendar.HOUR, 9);
//           calendar.set(Calendar.MINUTE, 0);
//           calendar.set(Calendar.SECOND, 0);
//
//           RestClient client = list().get(0);
//           client.setLatestMessage(calendar.getTime());
//
//           List<RestClient> clients = new ArrayList<RestClient>();
//           for (int index = 1; index < list().size(); index++) {
//               clients.add(list().get(index));
//           }
//           clients.add(client);
//
//           clientManager.setConnectedClients(clients);
//           PrintConnectedClients();
//       }catch (Exception e){
//           System.out.println(e.getMessage());
//       }
    }


//    @Ignore
//    public void testUpdateClient(){
//        RestClient client=blogService.getClientById("client-2");
//        client.setLatestActivity(new Date());
//        client.setClientRole(ClientRole.COMMENT.toString());
//        blogService.updateClient(client);
//    }
//
//    @Ignore
//    public void testRemoveClient() {
//        blogService.removeClient("client-2");
//    }


    @Test
    public void randomUser() throws Exception {
//        UserInfo userInfo = new UserInfo();
//
//        City city = blogService.getCityByName("Istanbul");
//
//        Address address = new Address();
//        address.setStreet("street");
//        address.setState("state");
//        address.setCity(city);
//        address.setZip("346662");
//
//        List<Phone> phones = new ArrayList<Phone>();
//
//        Phone phone = new Phone();
//        phone.setNum("12345");
//        phone.setType("work");
//
//        Phone phone2 = new Phone();
//        phone2.setNum("13456");
//        phone2.setType("work");
//
//        phones.add(phone);
//        phones.add(phone2);
//
//        userInfo.setName("Engin");
//        userInfo.setAddress(address);
//        //userInfo.getPhones().addAll(phones);
//
//        UserLoginInfo userLoginInfo = new UserLoginInfo();
//        userLoginInfo.setUsername("username");
//        userLoginInfo.setPassword("password");
//        userLoginInfo.setLastLoginTime(new Date());
//
//        user.setUserinfo(userInfo);
//        user.setUserLoginInfo(userLoginInfo);
//
//        Role admin=new Role();
//        Role postUser=new Role();
//
//        admin.setName("Admin");
//        postUser.setName("PostUser");
//
//        List<Role> roleList=new ArrayList<Role>();
//        roleList.add(admin);
//        roleList.add(postUser);

    }
}

//
//        bean.addUser(user,roleList,phones);

//Assert.assertEquals("engin","engin");
//clientManager.addClient("client-1","9090","engin","POST");