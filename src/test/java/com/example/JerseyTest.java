//package com.example;
//
//import com.example.entities.*;
//import com.example.rest.resource.CategoryResourceTest;
//import com.example.rest.resource.PostResource;
//import org.jboss.arquillian.container.test.api.Deployment;
//import org.jboss.arquillian.junit.Arquillian;
//import org.jboss.shrinkwrap.api.ShrinkWrap;
//import org.jboss.shrinkwrap.api.asset.EmptyAsset;
//import org.jboss.shrinkwrap.api.spec.WebArchive;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import javax.ws.rs.client.ClientBuilder;
//import javax.ws.rs.client.Entity;
//import javax.ws.rs.client.WebTarget;
//
//@RunWith(Arquillian.class)
//public class JerseyTest {
//
//    @Deployment
//    public static WebArchive createDeployment(){
//        return ShrinkWrap.create(WebArchive.class,"arquilian-demo-test.war")
//                .addClass(Client.class)
//                .addClass(Task.class)
//                .addClass(User.class)
//                .addClass(City.class)
//                .addClass(Country.class)
//                .addClass(Role.class)
//                .addClass(UserRole.class)
//                .addClass(Comment.class)
//                .addClass(Post.class)
//                .addClass(Document.class)
//                .addClass(Category.class)
//                .addClass(PostResource.class)
//                .addClass(CategoryResourceTest.class)
//                .addAsManifestResource("test-persistence.xml", "persistence.xml")
//                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
//                //.addAsWebInfResource("test-web.xml","web.xml")
//               // .addAsResource("test-persistence.xml","META-INF/persistence.xml");
//
//    }
//
//    @Test
//    public void testAddCategoryResource(){
//        WebTarget target= ClientBuilder.newClient()
//                .target("http://localhost:8080/arquilian-demo-test/services/category");
//        Category category=new Category();
//        category.setName("TEST");
//        target.request("application/json").post(Entity.json(category));
//    }
//}
