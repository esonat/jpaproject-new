package com.example;

import com.example.entities.Client;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by sonat on 30.08.2016.
 */


@RunWith(JUnit4.class)
public class TestRemove {

    @Test
    public void test(){
        List<Client> list=new ArrayList<Client>();
        Client client1=new Client("client-1",9010,true,null,"POST",new Date(),new Date());
        Client client2=new Client("client-2",9020,true,null,"POST",new Date(),new Date());
        Client client3=new Client("client-3",9030,true,null,"POST",new Date(),new Date());
        Client client4=new Client("client-4",9040,true,null,"POST",new Date(),new Date());

        list.add(client1);
        list.add(client2);
        list.add(client3);
        list.add(client4);

        List<String> ids=new ArrayList<String>();
        ids.add("client-1");
        ids.add("client-3");


        for (Iterator<Client> iterator = list.iterator(); iterator.hasNext();) {
            Client client= iterator.next();
            if (ids.contains(client.getId())) {
                // Remove the current element from the iterator and the list.
                iterator.remove();
            }
        }

        for(Client c:list){
            System.out.println(c.getId());
        }
    }

    @Ignore
    public void testList(){
        List<String> list=new ArrayList<String>();
        list.add("one");

        for(int index=0;index<list.size();index++){
            System.out.println(list.get(index));
        }
    }

}
