package com.example;

import com.example.connection.ClientManager;
import com.example.entities.*;
import com.example.message.MessageManager;
import com.example.service.BlogService;
import com.example.task.TaskManager;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(Arquillian.class)
public class TestTaskManager {

        @Deployment
        public static Archive<?> createDeployment() {
            //JavaArchive jar= ShrinkWrap.create(JavaArchive.class,"test.jar");

            JavaArchive jar = ShrinkWrap.create(
                    JavaArchive.class, "test.jar")
                    .addClass(Client.class)
                    .addClass(BlogService.class)
                    .addClass(ClientManager.class)
                    //.addClass(ApplicationManager.class)
                    .addClass(TaskManager.class)
                    .addClass(MessageManager.class)
                    .addClass(Task.class)
                    .addClass(User.class)
                    .addClass(City.class)
                    .addClass(Country.class)
                    .addClass(Role.class)
                    .addClass(UserRole.class)
                    .addClass(Comment.class)
                    .addClass(Post.class)
                    .addClass(Document.class)
                    .addClass(Category.class)
                    .addPackage(Phone.class.getPackage())
                    .addAsManifestResource("test-persistence.xml", "persistence.xml")
                    .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

            //   return jar;
            return jar;
        }

        @EJB
        BlogService blogService;

        @Inject
        ClientManager clientManager;

        @Inject
        TaskManager taskManager;

        public Client getClient() {
            Client client = new Client("client-1", 9090, true, null, "POST", new Date(), new Date());
            return client;
        }

        public List<Task> getTaskList(){

            List<Task> list=new ArrayList<Task>();

            Task roleTask=new Task();
            roleTask.setType(TaskType.TASK_SENDCLIENTROLEMESSAGE.toString());
            roleTask.setCategory(TaskCategory.MESSAGETASK.toString());
            roleTask.setLongRunning(false);
            //List<RestClient> clients=new ArrayList<RestClient>();
            //clients.add(getClient());
           // roleTask.setClients(clients);
            roleTask.setMessage(ClientRole.CATEGORY);
            roleTask.setMessageType(SentMessageType.CLIENTROLE);

            list.add(roleTask);
            return list;
        }

        public void PrintTasks(){
            for(Task task:taskManager.getTasks()){
                System.out.println(task.toString());
            }
        }

        @Ignore
        public void testCanAddTask(){
        try {
            Task roleTask = new Task();
            roleTask.setType(TaskType.TASK_SENDCATEGORYLISTMESSAGE.toString());
            roleTask.setCategory(TaskCategory.MESSAGETASK.toString());
            roleTask.setLongRunning(false);
            List<Client> clients = new ArrayList<Client>();
            clients.add(getClient());
            roleTask.setClients(clients);
            roleTask.setMessage(ClientRole.CATEGORY);
            roleTask.setMessageType(SentMessageType.CATEGORYLIST);

            taskManager.addTask(roleTask);
            //PrintTasks();

            Task categoryTask = new Task();
            categoryTask.setType(TaskType.TASK_SENDCLIENTROLEMESSAGE.toString());
            categoryTask.setCategory(TaskCategory.MESSAGETASK.toString());
            categoryTask.setLongRunning(false);
            //  List<RestClient> clients=new ArrayList<RestClient>();
            //clients.add(getClient());
            categoryTask.setClients(clients);
            //categoryTask.setMessage(ClientRole.CATEGORY);
            //categoryTask.setMessageType(SentMessageType.CLIENTROLE);
            taskManager.addTask(categoryTask);
            //PrintTasks();

            //System.out.println(roleTask.noConflict(categoryTask));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        }

        @Test
        public void testAddTask(){


          //  taskManager.addTask(roleTask);

        }

    @Test
    public void testGetClients(){
        List<Task> list=getTaskList();
        Task task=list.get(0);
        Client client=getClient();
        List<Client> listClients=new ArrayList<Client>();
        listClients.add(client);
        task.setClients(listClients);

        Assert.assertNotNull(task.getClients());
        System.out.println(task.getClients().get(0).getId());
    }
}
