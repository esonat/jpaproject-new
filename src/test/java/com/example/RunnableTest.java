package com.example;

import com.example.connection.ClientManager;
import com.example.entities.*;
import com.example.service.BlogService;
import com.example.task.TaskManager;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

/**
 * Created by sonat on 27.08.2016.
 */
@RunWith(Arquillian.class)
public class RunnableTest {

    @Deployment
    public static Archive<?> createDeployment(){
        //JavaArchive jar= ShrinkWrap.create(JavaArchive.class,"test.jar");

        JavaArchive jar= ShrinkWrap.create(
                JavaArchive.class,"test.jar")
                .addClass(Client.class)
                .addClass(BlogService.class)
                .addClass(ClientManager.class)
                //.addClass(ApplicationManager.class)
                .addClass(TaskManager.class)
                .addClass(Task.class)
                .addClass(User.class)
                .addClass(City.class)
                .addClass(Country.class)
                .addClass(Role.class)
                .addClass(UserRole.class)
                .addClass(Comment.class)
                .addClass(Post.class)
                .addClass(Document.class)
                .addClass(Category.class)
                .addPackage(Phone.class.getPackage())
                .addAsManifestResource("test-persistence.xml","persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");

        //   return jar;
        return jar;
    }

    @Test
    public void test(){
        try {
            InitialContext ctx = new InitialContext();
            NamingEnumeration<NameClassPair> list = ctx.list("");
            while (list.hasMore()) {
                System.out.println(list.next().getName());
            }
        }catch (NamingException e){

        }
    }

}
