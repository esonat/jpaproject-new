package com.example;

import com.example.connection.ClientManagerRemote;
import com.example.entities.*;
import com.example.service.BlogService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;
import java.util.Date;

/**
 * Created by sonat on 23.08.2016.
 */
@RunWith(Arquillian.class)
public class ClientManagerTest {


    @Deployment
    public static Archive<?> createDeployment(){
        //JavaArchive jar= ShrinkWrap.create(JavaArchive.class,"test.jar");

        JavaArchive jar= ShrinkWrap.create(
                JavaArchive.class,"test.jar")
                .addClass(Client.class)
                .addPackage(BlogService.class.getPackage())
                .addPackage(ClientManagerRemote.class.getPackage())
                .addClass(Task.class)
                .addClass(User.class)
                .addClass(City.class)
                .addClass(Country.class)
                .addClass(Role.class)
                .addClass(UserRole.class)
                .addClass(Comment.class)
                .addClass(Post.class)
                .addClass(Document.class)
                .addClass(Category.class)
                .addPackage(Phone.class.getPackage())
                .addAsManifestResource("test-persistence.xml","persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE,"beans.xml");

        //   return jar;
        return jar;
    }

//    @EJB
//    BlogServiceRemote blogService;

    @EJB
    ClientManagerRemote clientManager;

    public Client getClient() {
        Client client = new Client("client-1", 9090, true, null, "POST", new Date(), new Date());
        return client;
    }

    @Test
    public void testAddClient(){
        try {
            Client client = getClient();
            clientManager.addClient("client-1", "9090", "engin", "POST");
        }catch (Exception e){
            System.out.println("Exception");
        }
        Client item=null;
        try {
            item = clientManager.getClientById("client-1");
        }catch (Exception e){
            System.out.println("Exception in getClientById");
        }

        if(item!=null){
            System.out.println(item.getId()+item.getPort());
        }

    }

}
